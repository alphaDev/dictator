package com.alphadev.dictator.jira.item;

public class Component {
	private String	id;
	private String	name;
	private String	self;

	public String getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public String getSelf() {
		return self;
	}

	public void setId(String id) {
		this.id = id;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setSelf(String self) {
		this.self = self;
	}

}
