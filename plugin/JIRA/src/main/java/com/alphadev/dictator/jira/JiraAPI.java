package com.alphadev.dictator.jira;

import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.alphadev.dictator.Network;
import com.alphadev.dictator.dao.item.User;
import com.alphadev.dictator.jira.item.SearchIssueResponse;
import com.alphadev.dictator.properties.DictatorProperties;
import com.alphadev.dictator.properties.JIRAProperties;

public class JiraAPI {

	private static final DateFormat	JIRA_DATE_FORMATER	= new SimpleDateFormat("yyyy/MM/dd HH:mm");
	static final Logger				log					= LogManager.getLogger(JiraAPI.class);

	public static SearchIssueResponse getAssigneeProjet(Date since, int startIndex) {
		URL url = Network.makeURL(DictatorProperties.get(JIRAProperties.URL) + "rest/api/2/search");
		Map<String, String> arguments = new HashMap<String, String>();
		String filter = "assignee=" + DictatorProperties.get(JIRAProperties.LOGIN);

		if (since != null) {
			filter += " and Updated > \"" + JIRA_DATE_FORMATER.format(since) + "\"";
		}
		arguments.put("jql", filter);
		arguments.put("startAt", Integer.toString(startIndex));
//		arguments.put("maxResults", Integer.toString(2));

		return Network.getAndReadResponse(url, arguments, DictatorProperties.get(JIRAProperties.LOGIN), DictatorProperties.get(JIRAProperties.PASS), SearchIssueResponse.class);
	}

	public static SearchIssueResponse getWorkingProjet(User user) {
		URL url = Network.makeURL(DictatorProperties.get(JIRAProperties.URL) + "rest/api/2/search");
		Map<String, String> arguments = new HashMap<String, String>();
		arguments.put("jql", "assignee=" + DictatorProperties.get(JIRAProperties.LOGIN) + " AND  status = \"In Progress\"");

		SearchIssueResponse sr = Network.getAndReadResponse(url, arguments, DictatorProperties.get(JIRAProperties.LOGIN), DictatorProperties.get(JIRAProperties.PASS), SearchIssueResponse.class);
		return sr;
	}

	public static SearchIssueResponse getTodoProjet(User user) {
		URL url = Network.makeURL(DictatorProperties.get(JIRAProperties.URL) + "rest/api/2/search");
		Map<String, String> arguments = new HashMap<String, String>();
		arguments.put("jql", "assignee = \"" + DictatorProperties.get(JIRAProperties.LOGIN) + "\" AND (status = Open OR status = Reopened OR status = \"In Progress ( but on hold )\") ORDER BY status DESC, updatedDate DESC");

		SearchIssueResponse sr = Network.getAndReadResponse(url, arguments, DictatorProperties.get(JIRAProperties.LOGIN), DictatorProperties.get(JIRAProperties.PASS), SearchIssueResponse.class);
		return sr;
	}

	public static SearchIssueResponse getProjetInfo(User user, List<String> idProjets) {
		URL url = Network.makeURL(DictatorProperties.get(JIRAProperties.URL) + "rest/api/2/search");
		Map<String, String> arguments = new HashMap<String, String>();
		arguments.put("jql", String.format("key in (%s)", String.join(",", idProjets)));

		SearchIssueResponse sr = Network.getAndReadResponse(url, arguments, DictatorProperties.get(JIRAProperties.LOGIN), DictatorProperties.get(JIRAProperties.PASS), SearchIssueResponse.class);
		return sr;
	}

}
