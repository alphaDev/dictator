package com.alphadev.dictator.jira.item;

import java.util.List;


public class SearchIssueResponse {
	private String		expand;
	private List<Issue>	issues;
	private int			maxResults;
	private int			startAt;
	private int			total;

	public String getExpand() {
		return expand;
	}

	public List<Issue> getIssues() {
		return issues;
	}

	public int getMaxResults() {
		return maxResults;
	}

	public int getStartAt() {
		return startAt;
	}

	public int getTotal() {
		return total;
	}

	public void setExpand(String expand) {
		this.expand = expand;
	}

	public void setIssues(List<Issue> issues) {
		this.issues = issues;
	}

	public void setMaxResults(int maxResults) {
		this.maxResults = maxResults;
	}

	public void setStartAt(int startAt) {
		this.startAt = startAt;
	}

	public void setTotal(int total) {
		this.total = total;
	}

}
