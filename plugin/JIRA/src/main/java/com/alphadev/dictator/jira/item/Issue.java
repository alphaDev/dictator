package com.alphadev.dictator.jira.item;

public class Issue {
	private String	expand;
	private Fields	fields;
	private String	id;
	private String	key;
	private String	self;

	public String getExpand() {
		return expand;
	}

	public Fields getFields() {
		return fields;
	}

	public String getId() {
		return id;
	}

	public String getKey() {
		return key;
	}

	public String getSelf() {
		return self;
	}

	public void setExpand(String expand) {
		this.expand = expand;
	}

	public void setFields(Fields fields) {
		this.fields = fields;
	}

	public void setId(String id) {
		this.id = id;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public void setSelf(String self) {
		this.self = self;
	}

}
