package com.alphadev.dictator.jira;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.alphadev.dictator.dao.item.Session;
import com.alphadev.dictator.dao.item.Task;
import com.alphadev.dictator.dao.item.User;
import com.alphadev.dictator.dao.item.property.PropertyInfo;
import com.alphadev.dictator.jira.item.Issue;
import com.alphadev.dictator.jira.item.SearchIssueResponse;
import com.alphadev.dictator.plugin.Plugin;
import com.alphadev.dictator.plugin.PluginInfo;
import com.alphadev.dictator.properties.JIRAProperties;
import com.alphadev.dictator.properties.PropertyDefinition;

public class JiraPlugin implements Plugin {
	private static final Logger		LOG			= LogManager.getLogger(JiraPlugin.class);
	private static final PluginInfo	PLUGIN_INFO	= new PluginInfo("jira", "/com/alphadev/dictator/jira/jira-logo.png");

	@Override
	public PluginInfo getInfo() {
		return PLUGIN_INFO;
	}

	@Override
	public void init() {
		LOG.info("JiraPlugin started");
	}

	@Override
	public void setUser(User user) {
	}

	@Override
	public void updateUser(User user) {
	}

	@Override
	public List<PropertyInfo> askCustomProperties(Class<?> itemClass) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void syncTasks(List<Task> tasks, boolean syncAll, Date from, Date to) {
		SearchIssueResponse searchIssueResponse = null;
		int total = 0;
		do {
			searchIssueResponse = JiraAPI.getAssigneeProjet(from, total);

			for (Issue issue : searchIssueResponse.getIssues()) {
				Task taskToUpdate = tasks.stream().filter(t -> t.getKey().equals(issue.getKey())).findFirst().orElse(null);
				if (taskToUpdate == null) {
					taskToUpdate = new Task();
					taskToUpdate.setKey(issue.getKey());
					tasks.add(taskToUpdate);
					LOG.trace("creating new task [{}]", issue.getKey());
				}
				else {
					LOG.trace("updating new task [{}]", issue.getKey());
				}
				updateTask(taskToUpdate, issue);
			}

			total += searchIssueResponse.getIssues().size();
		}
		while (searchIssueResponse.getTotal() > total);
	}

	@Override
	public void syncSession(List<Task> tasks, List<Session> sessions, boolean syncAll, Date from, Date to) {
	}

	@Override
	public List<PropertyDefinition> getPropertyDefinition() {
		List<PropertyDefinition> propertyDefinitions = new ArrayList<>();
		propertyDefinitions.add(new PropertyDefinition(JIRAProperties.URL, true, PropertyDefinition.Type.STRING));
		propertyDefinitions.add(new PropertyDefinition(JIRAProperties.LOGIN, true, PropertyDefinition.Type.STRING));
		propertyDefinitions.add(new PropertyDefinition(JIRAProperties.PASS, true, PropertyDefinition.Type.STRING));

		for (String status : JIRAProperties.STATUS) {
			propertyDefinitions.add(new PropertyDefinition(status, false, PropertyDefinition.Type.LIST));
		}
		return propertyDefinitions;
	}

	@Override
	public Session createSession(Task task, Date start, Date end, String description) {
		return null;
	}

	@Override
	public Session closeSession(Session session) {
		return null;
	}

	@Override
	public Session updateSession(Session session) {
		return null;
	}

	@Override
	public void deleteSession(Session session) {
	}

	public void updateTask(Task task, Issue issue) {

		task.setKey(issue.getKey());
		task.setDescription(issue.getFields().getSummary());
		// String statusJira = issue.getFields().getStatus().getName();

		Date piLastChange = issue.getFields().getUpdated();
		Date lastChange = task.getLastChange();

		// LOG.debug("updateTask key[{}] updated [{}] statusJira[{}]", issue.getKey(), DateFormater.format(piLastChange), statusJira);

		if (lastChange == null || piLastChange.after(lastChange)) {
			task.setLastChange(piLastChange);
		}

		if (issue.getFields().getFixVersions().isEmpty()) {
			task.setVersion("No fix version set");
		}
		else {
			task.setVersion(issue.getFields().getFixVersions().get(0).getName());
		}

		task.syncDone(getInfo());
	}

}
