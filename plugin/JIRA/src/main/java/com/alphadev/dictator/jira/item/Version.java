package com.alphadev.dictator.jira.item;

public class Version {
	private boolean	archived;
	private String	id;
	private String	name;
	private boolean	released;
	private String	self;

	public String getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public String getSelf() {
		return self;
	}

	public boolean isArchived() {
		return archived;
	}

	public boolean isReleased() {
		return released;
	}

	public void setArchived(boolean archived) {
		this.archived = archived;
	}

	public void setId(String id) {
		this.id = id;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setReleased(boolean released) {
		this.released = released;
	}

	public void setSelf(String self) {
		this.self = self;
	}

}
