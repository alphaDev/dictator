package com.alphadev.dictator.properties;

public class JIRAProperties {
	public static final String		URL					= "jiraURL";
	public static final String		LOGIN				= "jiraLogin";
	public static final String		PASS				= "jiraPass";

	public static final String		STATUS_DESIGN		= "jiraStatusDESIGN";
	public static final String		STATUS_TO_START		= "jiraStatusTO_START";
	public static final String		STATUS_WORKING		= "jiraStatusWORKING";
	public static final String		STATUS_TO_TEST		= "jiraStatusTO_TEST";
	public static final String		STATUS_TESTING		= "jiraStatusTESTING";
	public static final String		STATUS_VALIDATE		= "jiraStatusVALIDATE";
	public static final String		STATUS_REJET		= "jiraStatusREJET";
	public static final String		STATUS_PAUSE		= "jiraStatusPAUSE";
	public static final String		STATUS_TO_REVIEW	= "jiraStatusTO_REVIEW";
	public static final String		STATUS_REVIEW		= "jiraStatusREVIEW";

	public static final String[]	STATUS				= {
	        STATUS_DESIGN,
	        STATUS_TO_START,
	        STATUS_WORKING,
	        STATUS_TO_TEST,
	        STATUS_TESTING,
	        STATUS_VALIDATE,
	        STATUS_REJET,
	        STATUS_PAUSE,
	        STATUS_TO_REVIEW,
	        STATUS_REVIEW
	};

}
