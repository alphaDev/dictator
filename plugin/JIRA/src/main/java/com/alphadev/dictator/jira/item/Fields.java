package com.alphadev.dictator.jira.item;

import java.util.Date;
import java.util.List;

public class Fields {
	private List<Component>	components;
	private List<Version>	fixVersions;
	private Status			status;
	private String			summary;
	private Date			updated;

	public List<Version> getFixVersions() {
		return fixVersions;
	}

	public String getSummary() {
		return summary;
	}

	public void setFixVersions(List<Version> fixVersions) {
		this.fixVersions = fixVersions;
	}

	public void setSummary(String summary) {
		this.summary = summary;
	}

	public List<Component> getComponents() {
		return components;
	}

	public void setComponents(List<Component> components) {
		this.components = components;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	public Date getUpdated() {
		return updated;
	}

	public void setUpdated(Date updated) {
		this.updated = updated;
	}
}
