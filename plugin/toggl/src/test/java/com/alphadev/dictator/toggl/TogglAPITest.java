package com.alphadev.dictator.toggl;

import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import org.junit.Ignore;
import org.junit.Test;

import com.alphadev.dictator.Network;
import com.alphadev.dictator.toggl.item.UserInfoResponse;

public class TogglAPITest {
//	private static final Logger LOG = LogManager.getLogger(TogglAPITest.class);

	@Test
	@Ignore
	public void testGetProjetsSumary() {
		// User user = User.getUser();
		// Map<String, ProjetInfo> projetTotalTime = TogglAPI.getProjetInfoById(user);
		//
		// for (Entry<String, ProjetInfo> projet : projetTotalTime.entrySet()) {
		// log.debug("p[{}] - [{}]", projet.getKey(), projet.getValue());
		// }
	}

	@Test
	@Ignore
	public void testGetUserInfo() {
		// User user = User.getUser();
		//// UserInfo uir = TogglAPI.getUserInfo(user);
		//
		// log.debug("uir Fullname [{}]", uir.getFullname());
		// log.debug("uir Default_wid [{}]", uir.getDefault_wid());
	}

	@Test
	@Ignore
	public void testGetCurrentTimeEntry() {
		// User user = User.getUser();
		// TimeEntry timeEntry = TogglAPI.getCurrentTimeEntry(user);
		//
		// log.debug("timeEntry [{}]", timeEntry);
	}

	@Test
	@Ignore
	public void testCreateCurrentTimeEntry() {
		// User user = User.getUser();
		//
		// TimeEntry timeEntry = new TimeEntry();
		// timeEntry.setDescription("testing");
		// timeEntry.setPid("24144748");
		// timeEntry.setCreated_with(Network.USER_AGENT);
		// TogglAPI.startCurrentTimeEntry(user, timeEntry);
		//
		// log.debug("timeEntry [{}]", timeEntry);
	}

	@Test
	@Ignore
	public void testGetWeekSummary() {
		// User user = User.getUser();
		//
		// List<TimeEntry> summary = TogglAPI.getWeekSummary(user);
		//
		// for (TimeEntry te : summary) {
		// log.debug("te [{}]", te);
		// }

	}

	@Test
	public void testUpdateTimeEntry() {
		URL url = Network.makeURL("https://toggl.com/api/v8/me");
		Map<String, String> arguments = new HashMap<String, String>();
		arguments.put("with_related_data", "true");
		Network.getAndReadResponse(url, arguments, "1dd6efe6d12843a859418f1a60b01bf6", "api_token", UserInfoResponse.class).getData();

	}
}
