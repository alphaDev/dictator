package com.alphadev.dictator.toggl.item;

import java.util.Date;

import com.alphadev.dictator.DurationFormater;

public class ProjetInfo {
	private long	totalTime;
	private String	color;
	private String	key;
	private String	pid;
	private Date	lastChange;

	@Override
	public String toString() {
		return String.format("/ProjetInfo[%s][%s] [%s]/", key, color, DurationFormater.format(totalTime));
	}

	public long getTotalTime() {
		return totalTime;
	}

	public String getKey() {
		return key;
	}

	public String getPid() {
		return pid;
	}

	public void setTotalTime(long totalTime) {
		this.totalTime = totalTime;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public void setPid(String pid) {
		this.pid = pid;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public Date getLastChange() {
		return lastChange;
	}

	public void setLastChange(Date lastChange) {
		this.lastChange = lastChange;
	}

}
