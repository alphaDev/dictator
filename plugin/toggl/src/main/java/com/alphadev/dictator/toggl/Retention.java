package com.alphadev.dictator.toggl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.alphadev.dictator.toggl.item.DetailData;

public class Retention {
	private Date				lastGet;
	private List<DetailData>	datas	= new ArrayList<>();

	public Date getLastGet() {
		return lastGet;
	}

	public void setLastGet(Date lastGet) {
		this.lastGet = lastGet;
	}

	public List<DetailData> getDatas() {
		return datas;
	}

	public void setDatas(List<DetailData> datas) {
		this.datas = datas;
	}

}
