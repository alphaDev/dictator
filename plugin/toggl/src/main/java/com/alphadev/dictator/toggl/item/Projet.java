package com.alphadev.dictator.toggl.item;


public class Projet {
	private String	project;
	private String	color;
	private String	hex_color;

	public String getProject() {
		return project;
	}

	public void setProject(String project) {
		this.project = project;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public String getHex_color() {
		return hex_color;
	}

	public void setHex_color(String hex_color) {
		this.hex_color = hex_color;
	}

}
