package com.alphadev.dictator.toggl;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.alphadev.dictator.Network;
import com.alphadev.dictator.dao.item.Session;
import com.alphadev.dictator.dao.item.Task;
import com.alphadev.dictator.dao.item.User;
import com.alphadev.dictator.dao.item.property.PropertyInfo;
import com.alphadev.dictator.plugin.Plugin;
import com.alphadev.dictator.plugin.PluginInfo;
import com.alphadev.dictator.properties.PropertyDefinition;
import com.alphadev.dictator.properties.TogglProperties;
import com.alphadev.dictator.toggl.item.DetailData;
import com.alphadev.dictator.toggl.item.ProjetInfo;
import com.alphadev.dictator.toggl.item.ReportsDetailsResponse;
import com.alphadev.dictator.toggl.item.TimeEntry;

public class TogglPlugin implements Plugin {

	private static final Logger		LOG			= LogManager.getLogger(TogglPlugin.class);
	private static final PluginInfo	PLUGIN_INFO	= new PluginInfo("toggl", "/com/alphadev/dictator/toggl/toggl-logo.png");

	@Override
	public PluginInfo getInfo() {
		return PLUGIN_INFO;
	}

	@Override
	public void init() {
		LOG.info("TogglPlugin started");
	}

	@Override
	public void setUser(User user) {
	}

	@Override
	public void updateUser(User user) {
	}

	@Override
	public List<PropertyInfo> askCustomProperties(Class<?> itemClass) {
		List<PropertyInfo> custProps = new ArrayList<>();
		if (itemClass.equals(Session.class)) {
			custProps.add(new PropertyInfo("tags", PropertyInfo.Type.STRING, getInfo()));
		}
		return custProps;
	}

	@Override
	public void syncTasks(List<Task> tasks, boolean syncAll, Date from, Date to) {
		Map<String, ProjetInfo> getProjetInfoByKey = TogglAPI.getProjetInfoByKey();
		for (ProjetInfo projetInfo : getProjetInfoByKey.values()) {
			Task taskToUpdate = tasks.stream().filter(t -> t.getKey().equals(projetInfo.getKey())).findFirst().orElse(null);
			if (taskToUpdate == null) {
				taskToUpdate = new Task();
				taskToUpdate.setKey(projetInfo.getKey());
				tasks.add(taskToUpdate);
			}
			taskToUpdate.setColor(projetInfo.getColor());
			taskToUpdate.setLastChange(projetInfo.getLastChange());
			taskToUpdate.syncDone(getInfo());

			LOG.trace("creating / updating task [{}]", projetInfo.getKey());
		}
	}

	@Override
	public void syncSession(List<Task> tasks, List<Session> sessions, boolean syncAll, Date from, Date to) {
		Map<String, ProjetInfo> getProjetInfoByKey = TogglAPI.getProjetInfoByKey();
		Map<String, Task> tasksByPid = tasks.stream().filter(t -> getProjetInfoByKey.get(t.getKey()) != null).collect(Collectors.toMap(t -> getProjetInfoByKey.get(t.getKey()).getPid(), t -> t));

		if (syncAll) {
			LOG.info("reload all entry");
			List<DetailData> found;
			Calendar tmp = Calendar.getInstance();

			do {
				to = tmp.getTime();
				tmp.add(Calendar.MONTH, -1);
				from = tmp.getTime();
				found = askingSession(from, to, sessions, tasksByPid);
				LOG.trace("syncAll asking since [{}] to [{}]", from, to);
			}
			while (found.size() > 0);
		}
		else {
			askingSession(from, to, sessions, tasksByPid);
			LOG.trace("sync asking since [{}] to [{}]", from, to);
		}

		TimeEntry currentTimeEntry = TogglAPI.getCurrentTimeEntry();
		if (currentTimeEntry != null) {
			Session sessionToUpdate = sessions.stream().filter(s -> s.getId().equals(currentTimeEntry.getId())).findFirst().orElse(null);
			if (sessionToUpdate == null) {
				sessionToUpdate = new Session();
				sessions.add(sessionToUpdate);
			}
			LOG.trace("current session found [{}]", sessionToUpdate);
			updateSession(sessionToUpdate, currentTimeEntry, tasksByPid.get(currentTimeEntry.getPid()));
		}
	}

	@Override
	public Session createSession(Task task, Date start, Date end, String description) {
		Map<String, ProjetInfo> getProjetInfoByKey = TogglAPI.getProjetInfoByKey();
		ProjetInfo pi = getProjetInfoByKey.get(task.getKey());

		if (pi == null) {
			pi = TogglAPI.createProjet(task.getKey());
		}

		TimeEntry timeEntry = new TimeEntry();
		timeEntry.setPid(pi.getPid());
		timeEntry.setStart(start);
		timeEntry.setStop(end);
		timeEntry.setDescription(description);
		timeEntry.setCreated_with(Network.USER_AGENT);
		timeEntry = TogglAPI.startCurrentTimeEntry(timeEntry);

		final Session session = new Session();
		updateSession(session, timeEntry, task);
		return session;
	}

	@Override
	public Session closeSession(Session session) {
		TimeEntry timeEntry = TogglAPI.endCurrentTimeEntry(session.getId());
		session.setStop(timeEntry.getStop());
		return session;
	}

	@Override
	public List<PropertyDefinition> getPropertyDefinition() {
		List<PropertyDefinition> propertyDefinitions = new ArrayList<>();
		propertyDefinitions.add(new PropertyDefinition(TogglProperties.URL, true, PropertyDefinition.Type.STRING));
		propertyDefinitions.add(new PropertyDefinition(TogglProperties.API_TOKEN, true, PropertyDefinition.Type.STRING));
		return propertyDefinitions;
	}

	@Override
	public Session updateSession(Session session) {
		Task task = session.task();
		TimeEntry timeEntry = new TimeEntry();
		if (task != null) {
			Map<String, ProjetInfo> getProjetInfoByKey = TogglAPI.getProjetInfoByKey();
			ProjetInfo pi = getProjetInfoByKey.get(task.getKey());

			if (pi == null) {
				pi = TogglAPI.createProjet(task.getKey());
			}

			timeEntry.setPid(pi.getPid());
		}

		timeEntry.setId(session.getId());
		timeEntry.setStart(session.getStart());
		timeEntry.setStop(session.getStop());
		if (session.close()) {
			final long duration = session.calcDuration() / 1000;
			timeEntry.setDuration(duration);
		}
		timeEntry.setDescription(session.getDescription());
		timeEntry.setCreated_with(Network.USER_AGENT);
		TogglAPI.updateTimeEntry(timeEntry);

		return session;
	}

	@Override
	public void deleteSession(Session session) {
		TogglAPI.deleteSession(session.getId());
	}

	private List<DetailData> askingSession(Date since, Date to, Collection<Session> sessions, Map<String, Task> tasksByPid) {
		ReportsDetailsResponse sessionsFormTask;
		List<DetailData> datas = new ArrayList<>();
		int total = 0;
		int page = 0;
		do {
			sessionsFormTask = TogglAPI.getSessions(since, to, page);
			datas.addAll(sessionsFormTask.getData());
			page++;
			total += sessionsFormTask.getData().size();
			Network.sleep(100);
		}
		while (sessionsFormTask.getTotal_count() > total);

		for (DetailData detailData : datas) {
			Session sessionToUpdate = sessions.stream().filter(s -> s.getId().equals(detailData.getId())).findFirst().orElse(null);
			if (sessionToUpdate == null) {
				sessionToUpdate = new Session();
				sessions.add(sessionToUpdate);
			}
			updateSession(sessionToUpdate, detailData, tasksByPid);

			LOG.trace("creating / updating session [{}]", sessionToUpdate);
		}
		return datas;
	}

	private void updateSession(Session session, TimeEntry timeEntry, Task task) {
		session.setDescription(timeEntry.getDescription());
		session.setId(timeEntry.getId());
		session.setLastChange(timeEntry.getAt());
		session.setStart(timeEntry.getStart());
		session.setStop(timeEntry.getStop());
		session.setTaskKey(task == null ? "" : task.getKey());
		if (timeEntry.getTags() != null) {
			session.setCustomValue("tags", String.join(",", timeEntry.getTags()), String.class);
		}
		session.syncDone(getInfo());
	}

	private void updateSession(Session session, DetailData data, Map<String, Task> tasksByPid) {
		session.setDescription(data.getDescription());
		session.setId(data.getId());
		session.setLastChange(data.getUpdated());
		session.setStart(data.getStart());
		session.setStop(data.getEnd());
		session.setTaskKey(tasksByPid.getOrDefault(data.getPid(), new Task()).getKey());
		session.setCustomValue("tags", String.join(",", data.getTags()), String.class);
		session.syncDone(getInfo());
	}

}
