package com.alphadev.dictator.toggl.item;


public class ProjetsSumaryItem {
	private Title	title;
	private long	time;

	public Title getTitle() {
		return title;
	}

	public void setTitle(Title title) {
		this.title = title;
	}

	public long getTime() {
		return time;
	}

	public void setTime(long time) {
		this.time = time;
	}

}
