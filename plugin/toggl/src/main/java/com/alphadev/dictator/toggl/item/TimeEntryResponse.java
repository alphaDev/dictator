package com.alphadev.dictator.toggl.item;


public class TimeEntryResponse {
	private TimeEntry data;

	public TimeEntry getData() {
		return data;
	}

	public void setData(TimeEntry data) {
		this.data = data;
	}
}
