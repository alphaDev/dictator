package com.alphadev.dictator.toggl;

import java.net.URL;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.alphadev.dictator.DurationFormater;
import com.alphadev.dictator.Network;
import com.alphadev.dictator.exception.DictatorRuntimeException;
import com.alphadev.dictator.properties.DictatorProperties;
import com.alphadev.dictator.properties.TogglProperties;
import com.alphadev.dictator.toggl.item.CreateProjetResponse;
import com.alphadev.dictator.toggl.item.ProjetInfo;
import com.alphadev.dictator.toggl.item.ProjetToggl;
import com.alphadev.dictator.toggl.item.ReportsDetailsResponse;
import com.alphadev.dictator.toggl.item.TimeEntry;
import com.alphadev.dictator.toggl.item.TimeEntryResponse;
import com.alphadev.dictator.toggl.item.UserInfo;
import com.alphadev.dictator.toggl.item.UserInfoResponse;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.util.ISO8601DateFormat;

public class TogglAPI {

	private static final DateFormat			TOGGL_DATE_FORMATER	= new ISO8601DateFormat();
	private static final Logger				LOG					= LogManager.getLogger(TogglAPI.class);

	private static UserInfo					userInfo;
	private static Map<String, ProjetInfo>	infoProjetsByKey;
	private static Map<String, ProjetInfo>	infoProjetsByPid;
	private static Date						weekTimeEntriesStart;
	private static List<TimeEntry>			weekTimeEntries;

	public static UserInfo getUserInfo() {
		if (userInfo == null) {
			LOG.debug("Get and store toggl user info");
			URL url = Network.makeURL(DictatorProperties.get(TogglProperties.URL) + "api/v8/me");
			Map<String, String> arguments = new HashMap<String, String>();
			userInfo = Network.getAndReadResponse(url, arguments, DictatorProperties.get(TogglProperties.API_TOKEN), "api_token", UserInfoResponse.class).getData();
		}
		return userInfo;
	}

	public static Map<String, ProjetInfo> getProjetInfoByKey() {
		if (TogglAPI.infoProjetsByKey == null) {
			getProjetInfo();
		}
		return infoProjetsByKey;
	}

	public static Map<String, ProjetInfo> getProjetInfoByPid() {
		if (TogglAPI.infoProjetsByPid == null) {
			getProjetInfo();
		}
		return infoProjetsByPid;
	}

	public static TimeEntry getCurrentTimeEntry() {
		URL url = Network.makeURL(DictatorProperties.get(TogglProperties.URL) + "api/v8/time_entries/current");
		Map<String, String> arguments = new HashMap<String, String>();

		TimeEntry timeEntry = Network.getAndReadResponse(url, arguments, DictatorProperties.get(TogglProperties.API_TOKEN), "api_token", TimeEntryResponse.class).getData();

		return timeEntry;
	}

	public static TimeEntry endCurrentTimeEntry(String timeEntryId) {
		URL url = Network.makeURL(DictatorProperties.get(TogglProperties.URL) + "api/v8/time_entries/" + timeEntryId + "/stop");
		Map<String, String> arguments = new HashMap<String, String>();

		TimeEntry te = Network.getAndReadResponse(url, arguments, DictatorProperties.get(TogglProperties.API_TOKEN), "api_token", TimeEntryResponse.class).getData();
		ProjetInfo projetInfo = getProjetInfoByPid().get(te.getPid());
		projetInfo.setTotalTime(projetInfo.getTotalTime() + te.calcDuration());

		List<TimeEntry> weekSummary = getWeekSummary();
		weekSummary.add(te);

		return te;
	}

	public static ProjetInfo endCurrentTimeEntry(String timeEntryId, Date at) {
		URL url = Network.makeURL(DictatorProperties.get(TogglProperties.URL) + "api/v8/time_entries/" + timeEntryId + "/stop");
		Map<String, String> arguments = new HashMap<String, String>();
		TimeEntry te = Network.getAndReadResponse(url, arguments, DictatorProperties.get(TogglProperties.API_TOKEN), "api_token", TimeEntryResponse.class).getData();

		Network.sleep(500);

		url = Network.makeURL(DictatorProperties.get(TogglProperties.URL) + "api/v8/time_entries/" + timeEntryId);
		String timeEntryJson = null;
		TimeEntry timeEntry = new TimeEntry();
		timeEntry.setStop(at);
		timeEntryJson = String.format("{\"time_entry\":{\"stop\":\"%s\",\"duration\":%s}}", TOGGL_DATE_FORMATER.format(at), (at.getTime() - te.getStart().getTime()) / 1000);
		te = Network.postAndReadResponse(url, timeEntryJson, DictatorProperties.get(TogglProperties.API_TOKEN), "api_token", TimeEntryResponse.class).getData();

		ProjetInfo projetInfo = getProjetInfoByPid().get(te.getPid());
		projetInfo.setTotalTime(projetInfo.getTotalTime() + te.calcDuration());

		List<TimeEntry> weekSummary = getWeekSummary();
		weekSummary.add(te);

		LOG.debug("endCurrentTimeEntry [{}]", te);

		return projetInfo;
	}

	public static TimeEntry updateTimeEntry(TimeEntry timeEntry) {
		URL url = Network.makeURL(DictatorProperties.get(TogglProperties.URL) + "api/v8/time_entries/" + timeEntry.getId());

		ObjectMapper mapper = Network.getObjectMapper();
		mapper.setDateFormat(TOGGL_DATE_FORMATER);

		String timeEntryJson = null;
		try {
			timeEntryJson = mapper.writeValueAsString(timeEntry);
			timeEntryJson = String.format("{\"time_entry\":%s}", timeEntryJson);

			LOG.info("timeEntryJson [{}]", timeEntryJson);
		}
		catch (JsonProcessingException e) {
			throw new DictatorRuntimeException(e);
		}
		TimeEntry te = Network.postAndReadResponse(url, timeEntryJson, DictatorProperties.get(TogglProperties.API_TOKEN), "api_token", TimeEntryResponse.class).getData();
		return te;
	}

	public static ReportsDetailsResponse getSessions(Date since, Date to, int page) {
		URL url = Network.makeURL(DictatorProperties.get(TogglProperties.URL) + "reports/api/v2/details");
		Map<String, String> arguments = new HashMap<String, String>();
		arguments.put("user_agent", Network.USER_AGENT);
		arguments.put("workspace_id", getUserInfo().getDefault_wid());
		arguments.put("page", Integer.toString(page));
		arguments.put("since", TOGGL_DATE_FORMATER.format(since));
		if (to != null) {
			arguments.put("until", TOGGL_DATE_FORMATER.format(to));
		}
		return Network.getAndReadResponse(url, arguments, DictatorProperties.get(TogglProperties.API_TOKEN), "api_token", ReportsDetailsResponse.class);
	}

	public static void deleteSession(String timeEntryId) {
		URL url = Network.makeURL(DictatorProperties.get(TogglProperties.URL) + "api/v8/time_entries/" + timeEntryId);
		Network.requestWithDataAndReadResponse(url, "", DictatorProperties.get(TogglProperties.API_TOKEN), "api_token", Object.class, "DELETE");
	}

	public static TimeEntry startCurrentTimeEntry(TimeEntry timeEntry) {
		URL url = Network.makeURL(DictatorProperties.get(TogglProperties.URL) + "api/v8/time_entries/start");
		ObjectMapper mapper = Network.getObjectMapper();
		String timeEntryJson = null;
		try {
			timeEntryJson = String.format("{\"time_entry\":%s}", mapper.writeValueAsString(timeEntry));
			return Network.postAndReadResponse(url, timeEntryJson, DictatorProperties.get(TogglProperties.API_TOKEN), "api_token", TimeEntryResponse.class).getData();
		}
		catch (JsonProcessingException e) {
			LOG.error(e);
			throw new DictatorRuntimeException(e);
		}
	}

	public static List<TimeEntry> getWeekSummary() {
		Calendar startDate = Calendar.getInstance();
		startDate.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
		startDate.set(Calendar.HOUR, 0);
		startDate.set(Calendar.MINUTE, 0);
		startDate.set(Calendar.SECOND, 0);
		startDate.set(Calendar.MILLISECOND, 0);
		startDate.add(Calendar.DATE, -1);

		if (weekTimeEntriesStart == null || weekTimeEntriesStart.compareTo(startDate.getTime()) != 0) {
			if (weekTimeEntriesStart != null) {
				LOG.debug("Get new getWeekSummary [{}]", weekTimeEntriesStart.compareTo(startDate.getTime()));
			}

			String startDateISO8601 = TOGGL_DATE_FORMATER.format(startDate.getTime());

			URL url = Network.makeURL(DictatorProperties.get(TogglProperties.URL) + "api/v8/time_entries");
			Map<String, String> arguments = new HashMap<String, String>();
			LOG.debug("getWeekSummary start_date [{}]", startDateISO8601);
			arguments.put("start_date", startDateISO8601);
			weekTimeEntries = new ArrayList<TimeEntry>(Arrays.asList(Network.getAndReadResponse(url, arguments, DictatorProperties.get(TogglProperties.API_TOKEN), "api_token", TimeEntry[].class)));
			weekTimeEntriesStart = startDate.getTime();
		}

		return weekTimeEntries;

	}

	public static ProjetInfo createProjet(String id) {
		LOG.debug("Creating projet [{}]", id);
		URL url = Network.makeURL(DictatorProperties.get(TogglProperties.URL) + "api/v8/projects");

		ProjetToggl projetTodo = new ProjetToggl();
		projetTodo.setName(id);
		projetTodo.setWid(getUserInfo().getDefault_wid());

		ObjectMapper mapper = Network.getObjectMapper();
		String postData;
		try {
			postData = String.format("{\"project\":%s}", mapper.writeValueAsString(projetTodo));
		}
		catch (JsonProcessingException e) {
			LOG.error(e);
			throw new DictatorRuntimeException(e);
		}
		ProjetToggl projetDone = Network.postAndReadResponse(url, postData, DictatorProperties.get(TogglProperties.API_TOKEN), "api_token", CreateProjetResponse.class).getData();

		ProjetInfo pi = new ProjetInfo();
		pi.setKey(projetDone.getName());
		pi.setPid(projetDone.getId());
		pi.setColor(projetDone.getHex_color());

		getProjetInfoByKey().put(pi.getKey(), pi);
		getProjetInfoByPid().put(pi.getPid(), pi);

		return pi;
	}

	private static void getProjetInfo() {
		LOG.debug("Get and store toggl projet total time info");

		UserInfo userInfo = getUserInfo();
		URL url = Network.makeURL(DictatorProperties.get(TogglProperties.URL) + "api/v8/workspaces/" + userInfo.getDefault_wid() + "/projects");
		Map<String, String> arguments = new HashMap<String, String>();
		ProjetToggl[] psr = Network.getAndReadResponse(url, arguments, DictatorProperties.get(TogglProperties.API_TOKEN), "api_token", ProjetToggl[].class);

		Map<String, ProjetInfo> bypid_infoProjets = new HashMap<>();
		Map<String, ProjetInfo> byid_infoProjets = new HashMap<>();
		for (ProjetToggl projet : psr) {
			ProjetInfo pi = new ProjetInfo();
			pi.setKey(projet.getName());
			pi.setPid(projet.getId());
			pi.setColor(projet.getHex_color());
			pi.setTotalTime(projet.getActual_hours() * DurationFormater.MILLIS_IN_HOURS);
			pi.setLastChange(projet.getAt());
			bypid_infoProjets.put(pi.getPid(), pi);
			byid_infoProjets.put(pi.getKey(), pi);
		}

		TogglAPI.infoProjetsByKey = byid_infoProjets;
		TogglAPI.infoProjetsByPid = bypid_infoProjets;
	}
}
