package com.alphadev.dictator.toggl.item;

import java.util.Date;
import java.util.List;

import com.alphadev.dictator.DurationFormater;

public class TimeEntry {
	private String			id;
	private String			wid;
	private String			pid;
	private ProjetInfo		projetInfo;
	private boolean			billable;
	private Date			start;
	private Date			stop;
	private long			duration;
	private String			description;
	private String			created_with;
	private Date			at;
	private List<String>	tags;

	@Override
	public String toString() {
		return String.format("/TE (%s:%s) (%tD-%tT:%tT) (%s)/", id, description, start, start, stop, DurationFormater.format(calcDuration()));
	}

	public long calcDuration() {
		if (duration != 0) {
			return duration * 1000;
		}
		if (stop == null && start != null) {
			return System.currentTimeMillis() - start.getTime();
		}
		return duration;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getWid() {
		return wid;
	}

	public void setWid(String wid) {
		this.wid = wid;
	}

	public String getPid() {
		return pid;
	}

	public void setPid(String pid) {
		this.pid = pid;
	}

	public boolean isBillable() {
		return billable;
	}

	public void setBillable(boolean billable) {
		this.billable = billable;
	}

	public Date getStart() {
		return start;
	}

	public void setStart(Date start) {
		this.start = start;
	}

	public Date getStop() {
		return stop;
	}

	public void setStop(Date stop) {
		this.stop = stop;
	}

	public long getDuration() {
		return duration;
	}

	public void setDuration(long duration) {
		this.duration = duration;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Date getAt() {
		return at;
	}

	public void setAt(Date at) {
		this.at = at;
	}

	public List<String> getTags() {
		return tags;
	}

	public void setTags(List<String> tags) {
		this.tags = tags;
	}

	public String getCreated_with() {
		return created_with;
	}

	public void setCreated_with(String created_with) {
		this.created_with = created_with;
	}

	public ProjetInfo getProjetInfo() {
		return projetInfo;
	}

	public void setProjetInfo(ProjetInfo projetInfo) {
		this.projetInfo = projetInfo;
	}

}
