package com.alphadev.dictator.toggl.item;

import java.util.List;


public class ProjetsSumaryResponse {
	private long				total_grand;
	private List<ProjetsSumary>	data;

	public long getTotal_grand() {
		return total_grand;
	}

	public void setTotal_grand(long total_grand) {
		this.total_grand = total_grand;
	}

	public List<ProjetsSumary> getData() {
		return data;
	}

	public void setData(List<ProjetsSumary> data) {
		this.data = data;
	}

}
