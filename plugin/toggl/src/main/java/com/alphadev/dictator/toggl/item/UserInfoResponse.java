package com.alphadev.dictator.toggl.item;


public class UserInfoResponse {

	private long		since;
	private UserInfo	data;

	public long getSince() {
		return since;
	}

	public void setSince(long since) {
		this.since = since;
	}

	public UserInfo getData() {
		return data;
	}

	public void setData(UserInfo data) {
		this.data = data;
	}

}
