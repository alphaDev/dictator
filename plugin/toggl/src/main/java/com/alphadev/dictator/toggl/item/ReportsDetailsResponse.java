package com.alphadev.dictator.toggl.item;

import java.util.List;

public class ReportsDetailsResponse {
	private Long				total_count;
	private Long				per_page;
	private List<DetailData>	data;

	public Long getTotal_count() {
		return total_count;
	}

	public void setTotal_count(Long total_count) {
		this.total_count = total_count;
	}

	public Long getPer_page() {
		return per_page;
	}

	public void setPer_page(Long per_page) {
		this.per_page = per_page;
	}

	public List<DetailData> getData() {
		return data;
	}

	public void setData(List<DetailData> data) {
		this.data = data;
	}

}
