package com.alphadev.dictator.toggl.item;

public class CreateProjetResponse {
	private ProjetToggl data;

	public ProjetToggl getData() {
		return data;
	}

	public void setData(ProjetToggl data) {
		this.data = data;
	}
}
