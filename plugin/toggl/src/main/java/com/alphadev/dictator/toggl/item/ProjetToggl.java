package com.alphadev.dictator.toggl.item;

import java.util.Date;

public class ProjetToggl {
	private String	id;
	private String	wid;
	private String	cid;
	private String	name;
	private boolean	billable;
	private boolean	is_private;
	private boolean	active;
	private Date	at;
	private long	template_id;
	private String	color;
	private String	hex_color;
	private int		actual_hours;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getWid() {
		return wid;
	}

	public void setWid(String wid) {
		this.wid = wid;
	}

	public String getCid() {
		return cid;
	}

	public void setCid(String cid) {
		this.cid = cid;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public boolean isBillable() {
		return billable;
	}

	public void setBillable(boolean billable) {
		this.billable = billable;
	}

	public boolean isIs_private() {
		return is_private;
	}

	public void setIs_private(boolean is_private) {
		this.is_private = is_private;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public Date getAt() {
		return at;
	}

	public void setAt(Date at) {
		this.at = at;
	}

	public long getTemplate_id() {
		return template_id;
	}

	public void setTemplate_id(long template_id) {
		this.template_id = template_id;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public String getHex_color() {
		return hex_color;
	}

	public void setHex_color(String hex_color) {
		this.hex_color = hex_color;
	}

	public int getActual_hours() {
		return actual_hours;
	}

	public void setActual_hours(int actual_hours) {
		this.actual_hours = actual_hours;
	}

}
