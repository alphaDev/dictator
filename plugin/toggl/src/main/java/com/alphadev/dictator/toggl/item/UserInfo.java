package com.alphadev.dictator.toggl.item;


public class UserInfo {
	private String	id;
	private String	api_token;
	private String	default_wid;
	private String	email;
	private String	fullname;
	private String	jquery_timeofday_format;
	private String	jquery_date_format;
	private String	timeofday_format;
	private String	date_format;
	private boolean	store_start_and_stop_time;
	private int		beginning_of_week;
	private String	language;
	private String	image_url;
	private boolean	sidebar_piechart;
	private String	at;
	private int		retention;
	private boolean	record_timeline;
	private boolean	render_timeline;
	private boolean	timeline_enabled;
	private boolean	timeline_experiment;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getApi_token() {
		return api_token;
	}

	public void setApi_token(String api_token) {
		this.api_token = api_token;
	}

	public String getDefault_wid() {
		return default_wid;
	}

	public void setDefault_wid(String default_wid) {
		this.default_wid = default_wid;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getFullname() {
		return fullname;
	}

	public void setFullname(String fullname) {
		this.fullname = fullname;
	}

	public String getJquery_timeofday_format() {
		return jquery_timeofday_format;
	}

	public void setJquery_timeofday_format(String jquery_timeofday_format) {
		this.jquery_timeofday_format = jquery_timeofday_format;
	}

	public String getJquery_date_format() {
		return jquery_date_format;
	}

	public void setJquery_date_format(String jquery_date_format) {
		this.jquery_date_format = jquery_date_format;
	}

	public String getTimeofday_format() {
		return timeofday_format;
	}

	public void setTimeofday_format(String timeofday_format) {
		this.timeofday_format = timeofday_format;
	}

	public String getDate_format() {
		return date_format;
	}

	public void setDate_format(String date_format) {
		this.date_format = date_format;
	}

	public boolean isStore_start_and_stop_time() {
		return store_start_and_stop_time;
	}

	public void setStore_start_and_stop_time(boolean store_start_and_stop_time) {
		this.store_start_and_stop_time = store_start_and_stop_time;
	}

	public int getBeginning_of_week() {
		return beginning_of_week;
	}

	public void setBeginning_of_week(int beginning_of_week) {
		this.beginning_of_week = beginning_of_week;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public String getImage_url() {
		return image_url;
	}

	public void setImage_url(String image_url) {
		this.image_url = image_url;
	}

	public boolean isSidebar_piechart() {
		return sidebar_piechart;
	}

	public void setSidebar_piechart(boolean sidebar_piechart) {
		this.sidebar_piechart = sidebar_piechart;
	}

	public String getAt() {
		return at;
	}

	public void setAt(String at) {
		this.at = at;
	}

	public int getRetention() {
		return retention;
	}

	public void setRetention(int retention) {
		this.retention = retention;
	}

	public boolean isRecord_timeline() {
		return record_timeline;
	}

	public void setRecord_timeline(boolean record_timeline) {
		this.record_timeline = record_timeline;
	}

	public boolean isRender_timeline() {
		return render_timeline;
	}

	public void setRender_timeline(boolean render_timeline) {
		this.render_timeline = render_timeline;
	}

	public boolean isTimeline_enabled() {
		return timeline_enabled;
	}

	public void setTimeline_enabled(boolean timeline_enabled) {
		this.timeline_enabled = timeline_enabled;
	}

	public boolean isTimeline_experiment() {
		return timeline_experiment;
	}

	public void setTimeline_experiment(boolean timeline_experiment) {
		this.timeline_experiment = timeline_experiment;
	}

}
