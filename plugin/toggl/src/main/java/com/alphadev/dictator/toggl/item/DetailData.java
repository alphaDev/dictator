package com.alphadev.dictator.toggl.item;

import java.util.Date;

import org.apache.commons.lang3.StringUtils;

import com.alphadev.dictator.DurationFormater;

public class DetailData {
	private String		id;
	private String		pid;
	private String		tid;
	private String		uid;
	private String		description;
	private Date		start;
	private Date		end;
	private Date		updated;
	private Long		dur;
	private String		user;
	private Boolean		use_stop;
	private String		client;
	private String		project;
	private String		project_color;
	private String		project_hex_color;
	private String		task;
	private String		billable;
	private String		is_billable;
	private String		cur;
	private String[]	tags;

	@Override
	public boolean equals(Object obj) {
		if (obj != null && obj instanceof DetailData) {
			return StringUtils.equals(id, ((DetailData) obj).id);
		}
		return false;
	}

	@Override
	public String toString() {
		return String.format("/S (%1$s) (%2$td/%2$tm/%2$ty-%2$tT) (%3$s)/", id, start, DurationFormater.format(dur));
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getPid() {
		return pid;
	}

	public void setPid(String pid) {
		this.pid = pid;
	}

	public String getTid() {
		return tid;
	}

	public void setTid(String tid) {
		this.tid = tid;
	}

	public String getUid() {
		return uid;
	}

	public void setUid(String uid) {
		this.uid = uid;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Date getStart() {
		return start;
	}

	public void setStart(Date start) {
		this.start = start;
	}

	public Date getEnd() {
		return end;
	}

	public void setEnd(Date end) {
		this.end = end;
	}

	public Date getUpdated() {
		return updated;
	}

	public void setUpdated(Date updated) {
		this.updated = updated;
	}

	public Long getDur() {
		return dur;
	}

	public void setDur(Long dur) {
		this.dur = dur;
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public Boolean getUse_stop() {
		return use_stop;
	}

	public void setUse_stop(Boolean use_stop) {
		this.use_stop = use_stop;
	}

	public String getClient() {
		return client;
	}

	public void setClient(String client) {
		this.client = client;
	}

	public String getProject() {
		return project;
	}

	public void setProject(String project) {
		this.project = project;
	}

	public String getProject_color() {
		return project_color;
	}

	public void setProject_color(String project_color) {
		this.project_color = project_color;
	}

	public String getProject_hex_color() {
		return project_hex_color;
	}

	public void setProject_hex_color(String project_hex_color) {
		this.project_hex_color = project_hex_color;
	}

	public String getTask() {
		return task;
	}

	public void setTask(String task) {
		this.task = task;
	}

	public String getBillable() {
		return billable;
	}

	public void setBillable(String billable) {
		this.billable = billable;
	}

	public String getIs_billable() {
		return is_billable;
	}

	public void setIs_billable(String is_billable) {
		this.is_billable = is_billable;
	}

	public String getCur() {
		return cur;
	}

	public void setCur(String cur) {
		this.cur = cur;
	}

	public boolean between(Date since, Date to) {
		return getStart().after(since) && (to == null || getStart().before(to));
	}

	public String[] getTags() {
		return tags;
	}

	public void setTags(String[] tags) {
		this.tags = tags;
	}

}
