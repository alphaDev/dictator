package com.alphadev.dictator.toggl.item;

import java.util.List;


public class ProjetsSumary {
	private String					id;
	private long					time;
	private Projet					title;
	private List<ProjetsSumaryItem>	items;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public long getTime() {
		return time;
	}

	public void setTime(long time) {
		this.time = time;
	}

	public Projet getTitle() {
		return title;
	}

	public void setTitle(Projet title) {
		this.title = title;
	}

	public List<ProjetsSumaryItem> getItems() {
		return items;
	}

	public void setItems(List<ProjetsSumaryItem> items) {
		this.items = items;
	}

}
