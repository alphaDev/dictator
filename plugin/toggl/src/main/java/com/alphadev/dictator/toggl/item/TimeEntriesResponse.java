package com.alphadev.dictator.toggl.item;

import java.util.List;


public class TimeEntriesResponse {
	private List<TimeEntry> data;

	public List<TimeEntry> getData() {
		return data;
	}

	public void setData(List<TimeEntry> data) {
		this.data = data;
	}

}
