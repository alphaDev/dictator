package com.alphadev.dictator.gmap;

import java.util.Date;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.alphadev.dictator.dao.item.Session;
import com.alphadev.dictator.dao.item.Task;
import com.alphadev.dictator.dao.item.User;
import com.alphadev.dictator.plugin.Plugin;
import com.alphadev.dictator.plugin.PluginInfo;
import com.alphadev.dictator.properties.PropertyDefinition;

public class GMapPlugin implements Plugin {

	private static final Logger		LOG			= LogManager.getLogger(GMapPlugin.class);
	private static final PluginInfo	PLUGIN_INFO	= new PluginInfo("gmap", "/com/alphadev/dictator/gmap/gmap-logo.png");

	@Override
	public PluginInfo getInfo() {
		return PLUGIN_INFO;
	}

	@Override
	public void init() {
		LOG.info("TogglPlugin started");
	}

	@Override
	public List<PropertyDefinition> getPropertyDefinition() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setUser(User user) {
		// TODO Auto-generated method stub

	}

	@Override
	public void updateUser(User user) {
		// TODO Auto-generated method stub

	}

	@Override
	public void syncTasks(List<Task> existingTasks, boolean syncAll, Date from, Date to) {
		// TODO Auto-generated method stub

	}

	@Override
	public void syncSession(List<Task> tasks, List<Session> sessions, boolean syncAll, Date from, Date to) {
		// TODO Auto-generated method stub

	}

	@Override
	public Session createSession(Task task, Date start, Date end, String description) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Session closeSession(Session session) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Session updateSession(Session session) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void deleteSession(Session session) {
		// TODO Auto-generated method stub

	}

}
