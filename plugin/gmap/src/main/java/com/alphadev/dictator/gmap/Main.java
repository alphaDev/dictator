package com.alphadev.dictator.gmap;

import java.io.IOException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.lynden.gmapsfx.GoogleMapView;
import com.lynden.gmapsfx.MapComponentInitializedListener;
import com.lynden.gmapsfx.javascript.object.DirectionsPane;
import com.lynden.gmapsfx.javascript.object.GoogleMap;
import com.lynden.gmapsfx.javascript.object.LatLong;
import com.lynden.gmapsfx.javascript.object.MapOptions;
import com.lynden.gmapsfx.javascript.object.MapTypeIdEnum;
import com.lynden.gmapsfx.service.directions.DirectionStatus;
import com.lynden.gmapsfx.service.directions.DirectionsGeocodedWaypoint;
import com.lynden.gmapsfx.service.directions.DirectionsLeg;
import com.lynden.gmapsfx.service.directions.DirectionsRenderer;
import com.lynden.gmapsfx.service.directions.DirectionsRequest;
import com.lynden.gmapsfx.service.directions.DirectionsResult;
import com.lynden.gmapsfx.service.directions.DirectionsRoute;
import com.lynden.gmapsfx.service.directions.DirectionsService;
import com.lynden.gmapsfx.service.directions.DirectionsServiceCallback;
import com.lynden.gmapsfx.service.directions.TravelModes;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

public class Main extends Application implements MapComponentInitializedListener, DirectionsServiceCallback {
	private static final Logger	LOG	= LogManager.getLogger(Main.class);

	private GoogleMapView		gmv;
	private DirectionsService	directionsService;
	private DirectionsPane		directionsPane;

	public static void main(String[] args) {
		launch(args);
	}

	@Override
	public void start(Stage stage) throws IOException {
		BorderPane root = new BorderPane();

		gmv = new GoogleMapView();
		gmv.addMapInializedListener(this);
		root.setCenter(gmv);
		stage.setScene(new Scene(root));

		stage.setTitle("gmap testing");
		stage.show();
	}

	@Override
	public void mapInitialized() {
		// Set the initial properties of the map.
		MapOptions mapOptions = new MapOptions();

		LatLong maison = new LatLong(45.002303, -0.620015);
		
		mapOptions.center(maison);
		mapOptions.mapType(MapTypeIdEnum.ROADMAP);
		mapOptions.overviewMapControl(false);
		// mapOptions.panControl(false);
		// mapOptions.rotateControl(false);
		// mapOptions.scaleControl(false);
		mapOptions.streetViewControl(false);
		mapOptions.zoomControl(true);
		mapOptions.zoom(12);

		GoogleMap map = gmv.createMap(mapOptions);
		map.showDirectionsPane();

		directionsService = new DirectionsService();
		directionsPane = gmv.getDirec();

		DirectionsRequest request = new DirectionsRequest("1 Passage de Rethondes 33460 Macau", "4 Avenue Descartes 33370 Artigues-près-Bordeaux", TravelModes.DRIVING);
		DirectionsRenderer directionsRenderer = new DirectionsRenderer(true, map, directionsPane);
		directionsService.getRoute(request, this, directionsRenderer);

	}

	@Override
	public void directionsReceived(DirectionsResult results, DirectionStatus status) {
		LOG.debug("status : [{}]", status.name());
		LOG.debug("getGeocodedWaypoints : [{}]", results.getGeocodedWaypoints());
		for (DirectionsGeocodedWaypoint geocodedWaypoints : results.getGeocodedWaypoints()) {
			LOG.debug("getGeocodedWaypoints : [{}]", geocodedWaypoints.getStatus());
		}
		for (DirectionsRoute route : results.getRoutes()) {
			for (DirectionsLeg legs : route.getLegs()) {
				LOG.debug("results getDuration : [{}]", legs.getDuration().getText());
				LOG.debug("results : [{}]", legs.getDuration().getJSObject());
			}
		}
	}
}
