package com.alphadev.dictator;

import java.io.IOException;

import com.alphadev.dictator.client.ui.StageInitializer;
import com.alphadev.dictator.properties.DesktopProperties;
import com.alphadev.dictator.properties.DictatorProperties;
import com.alphadev.dictator.properties.PropertyDefinition;

import javafx.application.Application;
import javafx.stage.Stage;

public class Main extends Application {

	public static void main(String[] args) {
		DictatorProperties.addPropertyDefinitions(new PropertyDefinition(DesktopProperties.IDLE_ACCEPT_TIME, false, PropertyDefinition.Type.DURATION));
		DictatorProperties.addPropertyDefinitions(new PropertyDefinition(DesktopProperties.CLOSE_WINDOWS_EXIT_APP, false, PropertyDefinition.Type.BOOL));
		DictatorProperties.addPropertyDefinitions(new PropertyDefinition(DesktopProperties.PLUGINS, false, PropertyDefinition.Type.LIST));
		DictatorProperties.addPropertyDefinitions(new PropertyDefinition(DesktopProperties.SKIN, false, PropertyDefinition.Type.STRING));
		DictatorProperties.addPropertyDefinitions(new PropertyDefinition(DesktopProperties.DATE_FORMAT, false, PropertyDefinition.Type.STRING));
		DictatorProperties.load();

		launch(args);

	}

	@Override
	public void start(Stage stage) throws IOException {
		StageInitializer.init(stage);
	}
}
