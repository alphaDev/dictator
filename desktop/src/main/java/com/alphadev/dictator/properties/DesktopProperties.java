package com.alphadev.dictator.properties;

public class DesktopProperties {
	public static final String	CLOSE_WINDOWS_EXIT_APP	= "closeWindowsExitApp";
	public static final String	PLUGINS					= "plugins";
	public static final String	SKIN					= "skin";
	public static final String	DATE_FORMAT				= "dateFormat";
	public static final String	IDLE_ACCEPT_TIME		= "idleAcceptTime";
}
