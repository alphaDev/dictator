package com.alphadev.dictator.dao;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Enumeration;
import java.util.List;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.alphadev.dictator.client.Backend;
import com.alphadev.dictator.dao.DataProvider;
import com.alphadev.dictator.dao.item.Session;
import com.alphadev.dictator.dao.item.Task;
import com.alphadev.dictator.dao.item.User;
import com.alphadev.dictator.dao.item.property.PropertyInfo;
import com.alphadev.dictator.plugin.Plugin;
import com.alphadev.dictator.plugin.PluginInfo;
import com.alphadev.dictator.properties.DesktopProperties;
import com.alphadev.dictator.properties.DictatorProperties;
import com.alphadev.dictator.properties.PropertyDefinition;

public class PluginsManager {
	private static final Logger			LOG					= LogManager.getLogger(Backend.class);
	private static final List<Plugin>	PLUGINS				= new ArrayList<>();
	private static final File			PLUGIN_FOLDER_PATH	= new File("plugins");

	public synchronized static void loadPlugins() {
		List<String> activePlugin = DictatorProperties.get(DesktopProperties.PLUGINS);
		LOG.debug("activePlugin [{}]", activePlugin);
		for (File file : PLUGIN_FOLDER_PATH.listFiles()) {
			if (!file.getName().endsWith(".jar")) {
				continue;
			}

			try {
				JarFile jarFile = new JarFile(file);
				Enumeration<JarEntry> e = jarFile.entries();

				URL[] urls = { file.toURI().toURL() };
				URLClassLoader cl = URLClassLoader.newInstance(urls);

				while (e.hasMoreElements()) {
					JarEntry je = e.nextElement();
					if (je.isDirectory() || !je.getName().endsWith(".class")) {
						continue;
					}
					String className = je.getName().substring(0, je.getName().length() - 6);
					className = className.replace('/', '.');
					Class<?> c = cl.loadClass(className);

					if (Plugin.class.isAssignableFrom(c)) {
						Plugin plugin = (Plugin) c.newInstance();
						PluginInfo pluginInfo = plugin.getInfo();
						if (activePlugin == null || activePlugin.contains(pluginInfo.getName())) {
							PLUGINS.add(plugin);
							LOG.info("Load plugin [{}]-[{}]-[{}]", file.getName(), c.getName(), pluginInfo.getName());
						}
						else {
							LOG.info("Skip plugin [{}]-[{}]-[{}]", file.getName(), c.getName(), pluginInfo.getName());
						}
					}
				}
				jarFile.close();
			}
			catch (IOException | InstantiationException | IllegalAccessException | ClassNotFoundException e) {
				LOG.debug("Plugin [{}] load fail.", file.getName(), e);
			}
		}
	}

	public synchronized static void askPropertiesNeed() {
		for (Plugin plugin : PLUGINS) {
			List<PropertyDefinition> propertyDefinitions = plugin.getPropertyDefinition();
			if (propertyDefinitions != null) {
				for (PropertyDefinition propertyDefinition : propertyDefinitions) {
					DictatorProperties.addPropertyDefinitions(propertyDefinition);
				}
			}
		}
	}

	public synchronized static void askCustomProperties() {
		for (Plugin plugin : PLUGINS) {
			List<PropertyInfo> customProperties = plugin.askCustomProperties(Session.class);
			if (customProperties != null && !customProperties.isEmpty()) {
				Session.setCustomProp(customProperties);
			}
		}
	}

	public synchronized static void initPlugins() {
		for (Plugin plugin : PLUGINS) {
			plugin.init();
		}
	}

	public synchronized static void setUser() {
		User user = User.getUser();
		for (Plugin plugin : PLUGINS) {
			plugin.setUser(user);
		}
	}

	public synchronized static void updateUser() {
		User user = User.getUser();
		for (Plugin plugin : PLUGINS) {
			plugin.updateUser(user);
		}
	}

	public synchronized static void syncTasks(boolean syncAll) {
		List<Task> tasksToSync = DataProvider.getTasks();
		Date since = null;
		Date to = new Date();
		if (syncAll) {
			tasksToSync.forEach(Task::askSync);
		}
		else {
			Calendar mintoask = Calendar.getInstance();
			mintoask.add(Calendar.DAY_OF_MONTH, -7);
			since = mintoask.getTime();
			Date sinceFinal = since;
			tasksToSync.stream().filter(t -> t.lastChangeBetween(sinceFinal, to)).forEach(Task::askSync);
		}

		for (Plugin plugin : PLUGINS) {
			plugin.syncTasks(tasksToSync, syncAll, since, to);
		}

		tasksToSync.removeIf(Task::wasNotSync);
		DataProvider.syncTasks(tasksToSync);
	}

	public synchronized static void syncSessions(boolean syncAll) {
		List<Session> sessionsToSync = DataProvider.getSessions();
		Date since = null;
		Date to = new Date();
		if (syncAll) {
			sessionsToSync.forEach(Session::askSync);
		}
		else {
			sessionsToSync.forEach(Session::resetSync);
			Calendar mintoask = Calendar.getInstance();
			mintoask.add(Calendar.DAY_OF_MONTH, -7);
			since = mintoask.getTime();
			Date sinceFinal = since;
			sessionsToSync.stream().filter(s -> s.lastChangeBetween(sinceFinal, to)).forEach(Session::askSync);
		}

		for (Plugin plugin : PLUGINS) {
			plugin.syncSession(DataProvider.getTasks(), sessionsToSync, syncAll, since, to);
		}

		sessionsToSync.removeIf(Session::wasNotSync);
		DataProvider.syncSessions(sessionsToSync);
	}

	public synchronized static Session createSession(Task task, Date start, Date end, String description) {
		for (Plugin plugin : PLUGINS) {
			Session session = plugin.createSession(task, start, end, description);
			if (session != null) {
				session.getFromPlugin().add(plugin.getInfo());
				DataProvider.createSession(session);
				return session;
			}
		}
		return null;
	}

	public synchronized static Session updateSession(Session session) {
		for (Plugin plugin : PLUGINS) {
			Session _session = plugin.updateSession(session);
			if (_session != null) {
				_session.getFromPlugin().add(plugin.getInfo());
				DataProvider.createSession(_session);
				return _session;
			}
		}
		return null;
	}

	public synchronized static Session closeSession(Session session) {
		for (Plugin plugin : PLUGINS) {
			Session _session = plugin.closeSession(session);
			if (_session != null) {
				_session.getFromPlugin().add(plugin.getInfo());
				DataProvider.createSession(_session);
				return _session;
			}
		}
		return session;
	}

	public synchronized static void deleteSession(Session session) {
		for (Plugin plugin : PLUGINS) {
			plugin.deleteSession(session);
		}
		DataProvider.deleteSession(session);
	}

	public static void main(String[] args) {
		PluginsManager.loadPlugins();
	}

}
