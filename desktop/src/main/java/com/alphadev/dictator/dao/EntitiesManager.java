package com.alphadev.dictator.dao;

import java.sql.SQLException;
import java.text.ParseException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.h2.tools.Console;
import org.hibernate.Hibernate;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;

public class EntitiesManager {

	private static final Logger		LOG				= LogManager.getLogger(EntitiesManager.class);
	private static SessionFactory	sessionFactory	= buildSessionFactory();

	private static SessionFactory buildSessionFactory() {
		try {
			if (sessionFactory == null) {
				Configuration configuration = new Configuration().configure(Hibernate.class.getResource("/hibernate.cfg.xml"));
				StandardServiceRegistryBuilder serviceRegistryBuilder = new StandardServiceRegistryBuilder();
				serviceRegistryBuilder.applySettings(configuration.getProperties());
				ServiceRegistry serviceRegistry = serviceRegistryBuilder.build();
				sessionFactory = configuration.buildSessionFactory(serviceRegistry);
			}
			return sessionFactory;
		}
		catch (Throwable ex) {
			LOG.error("Initial SessionFactory creation failed.", ex);
			throw new ExceptionInInitializerError(ex);
		}
	}

	public static SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	public static void shutdown() {
		getSessionFactory().close();
	}

	public static void main(String[] args) throws SQLException, ParseException {
		EntitiesManager.shutdown();
		Console.main(new String[] { "-url", "jdbc:h2:./dictator.db", "-user", "sa", "-password", "1" });
		// DatabaseManagerSwing.main(new String[] { "--url", "jdbc:hsqldb:file:jMoney.db", "--user", "sa", "--password", "1" });
		// Session session = EntryManager.sessionFactory.openSession();
		// Entry e = new Entry(new String[] { "24/02/2017", "for testing", "", "", "", "", "15.1" });
		// // Tag t = new Tag();
		// // t.setColor("#FF00FF");
		// // t.setName("paye");
		// // t.setExpression("VIR-");
		// // t.setUseRegEx(false);
		// // e.setTag(t);
		// session.beginTransaction();
		// // session.saveOrUpdate(t);
		// session.saveOrUpdate(e);
		// session.getTransaction().commit();
		// session.close();
		// EntitiesManager.shutdown();
		// Connection c = DriverManager.getConnection("jdbc:hsqldb:file:testing.db", "sa", "");
	}

}
