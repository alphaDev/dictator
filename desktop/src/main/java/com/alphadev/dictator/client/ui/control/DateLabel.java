package com.alphadev.dictator.client.ui.control;

import java.util.Date;

import com.alphadev.dictator.DateFormater;

import javafx.beans.property.StringProperty;
import javafx.beans.property.StringPropertyBase;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.scene.control.Label;

public class DateLabel extends Label {
	private static final String		DEFAULT_FORMAT	= "dd/MM/yyyy";

	private static StringProperty	staticFormat;
	private StringProperty			format;
	private Date					date;

	public DateLabel() {
		super();
		init();
	}

	public DateLabel(Date date) {
		super();
		init();
		setDate(date);
	}

	private void init() {
		getStyleClass().add("date-label");
		getFormatProperty().addListener(new ChangeListener<String>() {
			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				print();
			}
		});

		getFormatProperty().bind(getStaticFormat());
	}

	public static void setFormat(String format) {
		getStaticFormat().set(format);
	}

	public static String getFormat() {
		return getStaticFormat().get();
	}

	private static StringProperty getStaticFormat() {
		if (staticFormat == null) {
			staticFormat = new StringPropertyBase(DEFAULT_FORMAT) {

				@Override
				public String getName() {
					return "staticFormat";
				}

				@Override
				public Object getBean() {
					return null;
				}
			};
		}
		return staticFormat;
	}

	private StringProperty getFormatProperty() {
		if (format == null) {
			format = new StringPropertyBase(DEFAULT_FORMAT) {
				@Override
				public String getName() {
					return "dateFormat";
				}

				@Override
				public Object getBean() {
					return DateLabel.this;
				}

			};
		}
		return format;
	}

	public void setDateFormat(String format) {
		getFormatProperty().set(format);
	}

	public void setDate(Date date) {
		this.date = date;
		print();
	}

	private void print() {
		if (getDate() == null) {
			setText("");
		}
		else {
			setText(DateFormater.format(getFormat(), getDate()));
		}
	}

	public Date getDate() {
		return date;
	}

}
