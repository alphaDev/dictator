package com.alphadev.dictator.client.ui.component;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import com.alphadev.dictator.Constant;
import com.alphadev.dictator.DurationFormater;
import com.alphadev.dictator.client.ui.control.StackedBarChartWithMarker;
import com.alphadev.dictator.dao.DataListener;
import com.alphadev.dictator.dao.DataProvider;
import com.alphadev.dictator.dao.item.Day;
import com.alphadev.dictator.dao.item.Session;
import com.alphadev.dictator.dao.item.Task;
import com.alphadev.dictator.properties.CommonProperties;
import com.alphadev.dictator.properties.DictatorProperties;

import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.geometry.Insets;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.chart.XYChart.Data;
import javafx.scene.chart.XYChart.Series;
import javafx.scene.control.Tooltip;
import javafx.scene.layout.HBox;
import javafx.util.StringConverter;

public class SummaryChart extends HBox implements DataListener {

	private int											currentWeekSummary;
	private StackedBarChartWithMarker<String, Number>	weekSummaryGraph;

	public SummaryChart() {
		weekSummaryGraph = getWeekSummaryGraph();
		this.setPadding(Insets.EMPTY);
		getChildren().add(weekSummaryGraph);
		DataProvider.addDataListener(this);
	}

	private StackedBarChartWithMarker<String, Number> getWeekSummaryGraph() {
		CategoryAxis weekSummaryGraphCategoryAxis = new CategoryAxis();
		NumberAxis weekSummaryGraphHoursAxis = new NumberAxis();
		StackedBarChartWithMarker<String, Number> weekSummaryGraph = new StackedBarChartWithMarker<>(weekSummaryGraphCategoryAxis, weekSummaryGraphHoursAxis);

		weekSummaryGraphHoursAxis.setAutoRanging(false);
		weekSummaryGraphHoursAxis.setLowerBound(0);
		weekSummaryGraphHoursAxis.setUpperBound(1000 * 60 * 60 * 10);
		weekSummaryGraphHoursAxis.setTickLabelFormatter(new StringConverterToHours());
		weekSummaryGraphHoursAxis.setTickUnit(1000 * 60 * 60 * 2);
		weekSummaryGraphHoursAxis.setAnimated(false);
		List<String> dayCatName = Arrays.asList(Day.MONDAY.getCatName(), Day.TUESDAY.getCatName(), Day.WEDNESDAY.getCatName(), Day.THURSDAY.getCatName(), Day.FRIDAY.getCatName(), Day.SATURDAY.getCatName(), Day.SUNDAY.getCatName());
		weekSummaryGraphCategoryAxis.setCategories(FXCollections.<String> observableArrayList(dayCatName));
		weekSummaryGraph.getData().clear();
		weekSummaryGraph.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
		weekSummaryGraph.setAnimated(false);

		return weekSummaryGraph;
	}

	private class StringConverterToHours extends StringConverter<Number> {

		@Override
		public String toString(Number object) {
			long hours = object.longValue() / (DurationFormater.MILLIS_IN_HOURS);
			return hours + "h";
		}

		@Override
		public Number fromString(String string) {
			return Long.parseLong(string.substring(0, string.length() - 1)) * DurationFormater.MILLIS_IN_HOURS;
		}
	}

	@Override
	public void onSetCurrentSession(Session session) {

	}

	@Override
	public void onSetWorkingTasks(List<Task> tasks) {

	}

	@Override
	public void onSetTodayCloseSession(List<Session> todayCloseSession) {

	}

	@Override
	public void onSetWeekSummary(Map<Day, List<Session>> weekSummary) {
		if (weekSummary != null && currentWeekSummary != weekSummary.hashCode()) {
			Platform.runLater(() -> update(weekSummary));
		}
	}

	@Override
	public void onChangeSession(Map<String, Session> copySession) {

	}

	@Override
	public void onChangeTasks(Map<String, Task> copyTasks) {
	}

	private void update(Map<Day, List<Session>> weekSummary) {
		currentWeekSummary = weekSummary.hashCode();
		weekSummaryGraph.clearAllData();

		List<Series<String, Number>> series = new ArrayList<>();
		for (Day day : Day.values()) {
			String catName = day.getCatName();

			for (Session session : weekSummary.get(day)) {
				String taskKey = Constant.NO_PROJET_KEY;
				String taskDesc = Constant.NO_PROJET_DESC;
				String taskColor = Constant.NO_PROJET_COLOR;
				if (session.task() != null && StringUtils.isNotBlank(session.task().getKey())) {
					taskKey = session.task().getKey();
					taskColor = session.task().getColor();
					taskDesc = taskKey;
					if (StringUtils.isNotBlank(session.task().getDescription())) {
						taskDesc += " - " + session.task().getDescription();
					}
				}

				Series<String, Number> serie = new Series<>();
				series.add(serie);
				serie.setName(taskKey);
				Data<String, Number> data = new XYChart.Data<>(catName, session.calcDuration());
				data.setExtraValue(new String[] { taskDesc, taskColor });
				serie.getData().add(data);
			}
		}

		for (Series<String, Number> serie : series) {
			weekSummaryGraph.getData().add(serie);
		}

		for (Series<String, Number> serie : series) {
			for (Data<String, Number> data : serie.getData()) {
				if (data.getNode() != null) {
					String[] extraValue = (String[]) data.getExtraValue();
					data.getNode().setStyle(String.format("-fx-background-color: %s;", extraValue[1]));
					final Tooltip tooltip = new Tooltip();
					tooltip.setText(extraValue[0]);
					Tooltip.install(data.getNode(), tooltip);
				}
			}
		}

		Long workingHoursMarker = DictatorProperties.get(CommonProperties.WORKING_HOURS);
		Data<String, Number> marker = new Data<String, Number>(Day.MONDAY.getCatName(), workingHoursMarker);
		weekSummaryGraph.addHorizontalValueMarker(marker, "#FF0000");
	}
}
