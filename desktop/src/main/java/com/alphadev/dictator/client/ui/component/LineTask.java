package com.alphadev.dictator.client.ui.component;

import java.io.IOException;
import java.util.Calendar;

import com.alphadev.dictator.client.ui.StageInitializer;
import com.alphadev.dictator.client.ui.control.DurationLabel;
import com.alphadev.dictator.dao.PluginsManager;
import com.alphadev.dictator.dao.item.Session;
import com.alphadev.dictator.dao.item.Task;

import javafx.animation.AnimationTimer;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

public class LineTask extends VBox {

	@FXML
	private TaskColor		workingTaskColor;
	@FXML
	private Label			workingTaskInfoName;
	@FXML
	private Button			workingTaskInfoPause;
	@FXML
	private Button			workingTaskInfoStop;
	@FXML
	private Button			workingTaskInfoStart;
	@FXML
	private HBox			workingTaskButons;
	@FXML
	private HBox			sessionInfo;
	@FXML
	private DurationLabel	taskSession;
	@FXML
	private DurationLabel	taskTotal;
	@FXML
	private Button			sessionStart;
	@FXML
	private Button			sessionPause;

	private Session			openSession;
	private Task			task;

	public LineTask(Task task) {
		FXMLLoader fxmlLoader = new FXMLLoader(StageInitializer.CLASS_LOADER.getResource(StageInitializer.FXML_CURRENT_LINE_TASK));
		this.getStylesheets().add(StageInitializer.getStylesheet());
		fxmlLoader.setRoot(this);
		fxmlLoader.setController(this);
		try {
			fxmlLoader.load();
		}
		catch (IOException exception) {
			throw new RuntimeException(exception);
		}

		updateTask(task);

		LineTask local = this;
		(new AnimationTimer() {
			@Override
			public void handle(long now) {
				if (local.isVisible()) {
					if (openSession != null) {
						taskSession.setDuration(openSession.calcDuration());
					}
				}
				else {
					this.stop();
				}
			}
		}).start();
	}

	public void onSessionStart() {
		openSession = PluginsManager.createSession(task, Calendar.getInstance().getTime(), null, "");
	}

	public void onSessionPause() {
		if (openSession != null) {
			PluginsManager.closeSession(openSession);
		}
	}

	public void onWorkingTaskInfoStartAction() {
	}

	public void onWorkingTaskInfoPauseAction() {
	}

	public void onWorkingTaskInfoStopAction() {

	}

	private void updateTask(Task task) {
		this.task = task;
		workingTaskColor.setTask(task);

		workingTaskInfoName.setText(task.getKey() + "-" + task.getDescription());
		workingTaskInfoName.getTooltip().setText(task.getKey() + "-" + task.getDescription());
		taskTotal.setDuration(task.calcTimeSpend());

		workingTaskButons.getChildren().clear();

		sessionInfo.getChildren().remove(sessionStart);
		sessionInfo.getChildren().remove(sessionPause);
		openSession = task.openSession();
		if (openSession == null) {
			taskSession.setText("Session not start");
			sessionInfo.getChildren().add(sessionStart);
		}
		else {
			taskSession.setDuration(openSession.calcDuration());
			sessionInfo.getChildren().add(sessionPause);
		}
	}
}
