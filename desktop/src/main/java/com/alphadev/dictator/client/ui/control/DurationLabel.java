package com.alphadev.dictator.client.ui.control;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.alphadev.dictator.DurationFormater;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.css.CssMetaData;
import javafx.css.StyleConverter;
import javafx.css.Styleable;
import javafx.css.StyleableProperty;
import javafx.css.StyleableStringProperty;
import javafx.scene.control.Label;

public class DurationLabel extends Label {
	private static final String								CSS_OPTION_NAME	= "-fx-duration-format";
	private static final String								DEFAULT_FORMAT	= "%oHh%%poMm%%poSs%";

	private static final CssMetaData<DurationLabel, String>	DURATION_FORMAT	= new CssMetaData<DurationLabel, String>(CSS_OPTION_NAME, StyleConverter.getStringConverter(), DEFAULT_FORMAT) {

																				@Override
																				public boolean isSettable(DurationLabel styleable) {
																					return true;
																				}

																				@Override
																				public StyleableProperty<String> getStyleableProperty(DurationLabel styleable) {
																					return styleable.getDurationFormatProperty();
																				}
																			};

	private StyleableStringProperty							durationFormat;

	private long											duration;

	public DurationLabel() {
		super();
		init();
	}

	public DurationLabel(long duration) {
		super();
		init();
		setDuration(duration);
	}

	private void init() {
		getStyleClass().add("duration-label");
		getDurationFormatProperty().addListener(new ChangeListener<String>() {
			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				setText(DurationFormater.formatDuration(newValue, duration));
			}
		});
	}

	private StyleableStringProperty getDurationFormatProperty() {
		if (durationFormat == null) {
			durationFormat = new StyleableStringProperty(DEFAULT_FORMAT) {
				@Override
				public String getName() {
					return "durationFormat";
				}

				@Override
				public Object getBean() {
					return DurationLabel.this;
				}

				@Override
				public CssMetaData<DurationLabel, String> getCssMetaData() {
					return DURATION_FORMAT;
				}
			};
		}
		return durationFormat;
	}

	public String getDurationFormat() {
		return getDurationFormatProperty().get();
	}

	public void setDurationFormat(String format) {
		getDurationFormatProperty().set(format);
	}

	public void setDuration(long duration) {
		this.duration = duration;
		setText(DurationFormater.formatDuration(getDurationFormatProperty().get(), duration));
	}

	@Override
	public List<CssMetaData<? extends Styleable, ?>> getControlCssMetaData() {
		return STYLEABLE_PROPERTIES;
	}

	private static final List<CssMetaData<? extends Styleable, ?>> STYLEABLE_PROPERTIES;
	static {
		final List<CssMetaData<? extends Styleable, ?>> styleables = new ArrayList<>(Label.getClassCssMetaData());
		Collections.addAll(styleables, DURATION_FORMAT);
		STYLEABLE_PROPERTIES = Collections.unmodifiableList(styleables);
	}

}
