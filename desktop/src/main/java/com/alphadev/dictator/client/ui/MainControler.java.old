package com.alphadev.dictator.client.ui;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.alphadev.dictator.Network;
import com.alphadev.dictator.PluginsManager;
import com.alphadev.dictator.Utils;
import com.alphadev.dictator.client.MainListener;
import com.alphadev.dictator.client.MainThread;
import com.alphadev.dictator.client.TimeAction;
import com.alphadev.dictator.client.ui.component.ControlerMultiTaskSelector;
import com.alphadev.dictator.client.ui.weekchart.WeekChart;
import com.alphadev.dictator.item.Session;
import com.alphadev.dictator.item.Task;
import com.alphadev.dictator.item.User;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

public class MainControler implements Initializable, MainListener {
	private static final String			EMPTY_CHAR			= "\u00D8";
	private static final int			STAGE_MAX_HEIGHT	= 250;
	private static final int			STAGE_MIN_HEIGHT	= 78;

	private static final Logger			log					= LogManager.getLogger(MainControler.class);
	private static boolean				runThread;

	private Stage						stage;
	private List<Node>					noWorking			= new ArrayList<>();
	private List<Node>					working				= new ArrayList<>();
	private Session						currentSession;
	private ModalControler				modalControler;
	private Image						imageMinus;
	private Image						imagePlus;

	private ControlerMultiTaskSelector	controlerMultiTaskSelector;
	private VBox						multiTaskSelector;

	@FXML
	private VBox						rootMain;
	@FXML
	private Insets						defaultInsets;
	@FXML
	private Text						headerCurrentWorkingDayTime;
	@FXML
	private Text						headerUserName;
	@FXML
	private Text						headerVersion;
	@FXML
	private VBox						workingTaskInfo;
	@FXML
	private Label						workingTaskInfoName;
	@FXML
	private Text						workingTaskInfoSession;
	@FXML
	private Text						workingTaskInfoTotal;
	@FXML
	private HBox						workingTaskButons;
	@FXML
	private Button						workingTaskInfoStart;
	@FXML
	private Button						workingTaskInfoPause;
	@FXML
	private Button						workingTaskInfoStop;
	@FXML
	private HBox						noWorkingTask;
	@FXML
	private HBox						message;
	@FXML
	private Text						messageText;
	@FXML
	private WeekChart					weekChart;
	@FXML
	private Button						plusMinusWindowAction;
	@FXML
	private Circle						workingTaskColor;

	@Override
	public void initialize(URL url, ResourceBundle rb) {
		runThread = true;
		imageMinus = new Image(MainControler.class.getResourceAsStream("/com/alphadev/dictator/client/ui/img/minus.png"));
		imagePlus = new Image(MainControler.class.getResourceAsStream("/com/alphadev/dictator/client/ui/img/plus.png"));
		MainThread.addMainListeners(this);
		new UIThread().start();
		headerCurrentWorkingDayTime.setText(EMPTY_CHAR);

		rootMain.getChildren().remove(multiTaskSelector);
		for (Node node : workingTaskInfo.getChildren()) {
			if (StringUtils.equals(node.getId(), "noWorkingTask")) {
				noWorking.add(node);
			}
			else if (!StringUtils.equals(node.getId(), "message")) {
				working.add(node);
			}
		}

		User user = User.getUser();
//		headerUserName.setText(user.getName());

		setWorkingTask();
	}

	@Override
	public void onWorkingTaskChange(Task currentTasks) {
		Platform.runLater(() -> {
			setWorkingTask();
			StageInitializer.showSystrayMessage(String.format("New working task [%s] - %s", currentTasks.getId(), currentTasks.getDescription()));
		});
	}

	@Override
	public void onSessionChange(Session currentSession) {
		Platform.runLater(() -> {
			setSessionTime(currentSession);
		});
	}

	@Override
	public void onTotalTaskTimeChange(Task currentTask) {
		Platform.runLater(() -> {
			setTotalTime(currentTask);
		});
	}

	@Override
	public void onWake(long wakeTime) {
		Platform.runLater(() -> {
			if (modalControler != null) {
				modalControler.setWakeMillis(wakeTime);
			}
		});
	}

	@Override
	public void onIdle(long idleStartMillis) {
		Platform.runLater(() -> {
			showModalIdle(idleStartMillis);
		});
	}

	@Override
	public void onExceptionMessage(String _message) {
		Platform.runLater(() -> {
			if (StringUtils.isBlank(_message)) {
				workingTaskInfo.getChildren().remove(message);
			}
			else {
				messageText.setText(_message);
				if (!workingTaskInfo.getChildren().contains(message)) {
					workingTaskInfo.getChildren().add(0, message);
				}
			}
		});
	}

	@Override
	public void onMultiTask(List<Task> tasks) {
		Platform.runLater(() -> {
			if (tasks.size() > 1) {
				if (!rootMain.getChildren().contains(multiTaskSelector)) {
					controlerMultiTaskSelector.prepare(tasks);
					rootMain.getChildren().add(1, multiTaskSelector);
					rootMain.getChildren().remove(workingTaskInfo);
				}
			}
			else {
				rootMain.getChildren().remove(multiTaskSelector);
				if (!rootMain.getChildren().contains(workingTaskInfo)) {
					rootMain.getChildren().add(workingTaskInfo);
				}
			}
		});
	}

	public void onWorkingTaskInfoStartAction(ActionEvent event) {
		TimeAction timeAction = PluginsManager.getTimeAction();
		User user = User.getUser();
		timeAction.startCurrentTask(user, user.getCurrentTask());
	}

	public void onWorkingTaskInfoStopAction(ActionEvent event) {

	}

	public void onWorkingTaskInfoPauseAction(ActionEvent event) {
		TimeAction timeAction = PluginsManager.getTimeAction();
		User user = User.getUser();
		timeAction.pauseCurrentTask(user, user.getCurrentTask());
	}

	@FXML
	public void onCloseWindowAction(ActionEvent event) {
//		if (DictatorProperties.getCloseWindowsExitApp()) {
//			stage.getOnCloseRequest().handle(null);
//		}
//		else {
//			stage.hide();
//		}
	}

	@FXML
	public void onPlusMinusWindowAction(ActionEvent event) {
		if (stage.getHeight() == STAGE_MIN_HEIGHT) {
			stage.setHeight(STAGE_MAX_HEIGHT);
			plusMinusWindowAction.setGraphic(new ImageView(imageMinus));
		}
		else {
			stage.setHeight(STAGE_MIN_HEIGHT);
			plusMinusWindowAction.setGraphic(new ImageView(imagePlus));
		}
		javafx.scene.shape.Rectangle rect = new javafx.scene.shape.Rectangle(stage.getWidth(), stage.getHeight());
		rect.setArcHeight(40.0);
		rect.setArcWidth(40.0);

		stage.getScene().getRoot().setClip(rect);
	}

	public void setStage(Stage stage) {
		this.stage = stage;
	}

	public void setControlerMultiTaskSelector(ControlerMultiTaskSelector controlerMultiTaskSelector) {
		this.controlerMultiTaskSelector = controlerMultiTaskSelector;
	}

	public void setMultiTaskSelector(VBox multiTaskSelector) {
		this.multiTaskSelector = multiTaskSelector;
	}

	private void setWorkingTask() {
		User user = User.getUser();
//		log.debug("UI display working task {}", user.getName());
		workingTaskInfo.getChildren().remove(message);
		rootMain.getChildren().remove(multiTaskSelector);

		Task currentTask = user.getCurrentTask();

		// currentTask = new Task();
		// currentTask.setId("SEF-1212");
		// currentTask.setVersion("Delivery V02.03.03");
		// currentTask.setColor("00FF00");
		// // currentTask.setDescription("qdzidqdzqjoidjzqomidjzqoidjssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssszqoidjzqoidjqoidjzqoidqzoidzoiqjdoi");
		// currentTask.setDescription("KIKI");
		// currentTask.setTimeSpend(15445454841l);
		// Session session = new Session();
		// try {
		// session.setStart(DateFormat.getDateInstance(DateFormat.MEDIUM, Locale.US).parse("Nov 12, 2016"));
		// }
		// catch (ParseException e) {
		// e.printStackTrace();
		// }
		// currentTask.setSession(currentSession);

		if (currentTask == null) {
			workingTaskInfo.getChildren().removeAll(working);
			workingTaskInfo.getChildren().removeAll(noWorking);
			workingTaskInfo.getChildren().addAll(noWorking);
			headerVersion.setText("");
		}
		else {
			workingTaskInfo.getChildren().removeAll(working);
			workingTaskInfo.getChildren().removeAll(noWorking);
			workingTaskInfo.getChildren().addAll(working);

			workingTaskInfoName.setText(currentTask.getId() + "-" + currentTask.getDescription());
			workingTaskInfoName.getTooltip().setText(currentTask.getId() + "-" + currentTask.getDescription());
			log.debug("currentTask.getColor() [{}]", currentTask.getColor());
			if (StringUtils.isBlank(currentTask.getColor())) {
				workingTaskColor.setVisible(false);
				workingTaskColor.setRadius(0);
			}
			else {
				workingTaskColor.setVisible(true);
				workingTaskColor.setRadius(7);
				workingTaskColor.setFill(Paint.valueOf(currentTask.getColor()));
			}
			Utils.hackTooltipStartTiming(workingTaskInfoName.getTooltip(), 10);

//			Session currentSession = currentTask.getSession();
//			setSessionTime(currentSession);

			setTotalTime(currentTask);
			headerVersion.setText(currentTask.getVersion());

		}

		weekChart.update();
	}

	private void setTotalTime(Task currentTask) {
//		User user = User.getUser();
//		if (currentTask.getTimeSpend() <= 0) {
//			workingTaskInfoTotal.setText(EMPTY_CHAR);
//		}
//		else {
//			workingTaskInfoTotal.setText(Utils.formatDurationHourAndWorking(currentTask.getTimeSpend()));
//		}

//		if (user.getCurrentWorkingDayTime() <= 0) {
//			headerCurrentWorkingDayTime.setText(EMPTY_CHAR);
//		}
//		else {
//			headerCurrentWorkingDayTime.setText(Utils.formatDurationOnlyHours(user.getCurrentWorkingDayTime()));
//		}

	}

	private void setSessionTime(Session currentSession) {
		workingTaskButons.getChildren().clear();
		if (currentSession == null) {
			workingTaskInfoSession.setText("Session not start");
			workingTaskButons.getChildren().add(workingTaskInfoStart);
		}
		else {
			workingTaskInfoSession.setText(Utils.formatDuration(currentSession.calcDuration()));
			workingTaskButons.getChildren().add(workingTaskInfoPause);
			workingTaskButons.getChildren().add(workingTaskInfoStop);
		}
		weekChart.update();
		this.currentSession = currentSession;
	}

	private void showModalIdle(long idleStartMillis) {
		if (!ModalControler.idlePopup && currentSession != null) {
			try {
				Stage dialog = new Stage();
				FXMLLoader loader = new FXMLLoader(getClass().getClassLoader().getResource("com/alphadev/dictator/client/ui/idleModal.fxml"));
				Parent root = loader.load();
				modalControler = loader.<ModalControler> getController();
				modalControler.setIdleStartMillis(idleStartMillis);
				root.setId("rootModal");
				Scene scene = new Scene(root);
				scene.getStylesheets().setAll(getClass().getClassLoader().getResource("com/alphadev/dictator/client/ui/idleModal.css").toString());
				scene.setFill(Color.TRANSPARENT);

				dialog.setWidth(300d);
				dialog.setHeight(200d);
				dialog.setScene(scene);
				dialog.initOwner(stage);
				dialog.initModality(Modality.APPLICATION_MODAL);
				dialog.initStyle(StageStyle.TRANSPARENT);

				Rectangle rect = new Rectangle(dialog.getWidth(), dialog.getHeight());
				rect.setArcHeight(40.0);
				rect.setArcWidth(40.0);
				root.setClip(rect);

				ModalControler.idlePopup = true;

				dialog.show();
			}
			catch (IOException e) {
				StageInitializer.critalError("Cannot open idle modal.", e);
			}
		}
	}

	private class UIThread extends Thread {
		@Override
		public void run() {
			User user = User.getUser();
			while (runThread) {
				if (currentSession != null) {
//					long workingDayDuration = user.getCurrentWorkingDayTime() + currentSession.getDuration();
//					Platform.runLater(() -> workingTaskInfoSession.setText(Utils.formatDurationOnlyHours(currentSession.getDuration())));
//					if (workingDayDuration > 0) {
//						Platform.runLater(() -> headerCurrentWorkingDayTime.setText(Utils.formatDurationOnlyHours(workingDayDuration)));
//					}
				}
				Network.sleep(1000);
			}
		}
	}

	public static void stopThread() {
		runThread = false;
	}
}
