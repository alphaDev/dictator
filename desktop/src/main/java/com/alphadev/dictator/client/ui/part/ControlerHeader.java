package com.alphadev.dictator.client.ui.part;

import java.net.URL;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.alphadev.dictator.client.Backend;
import com.alphadev.dictator.client.ui.StageInitializer;
import com.alphadev.dictator.client.ui.control.DurationLabel;
import com.alphadev.dictator.dao.DataListener;
import com.alphadev.dictator.dao.DataProvider;
import com.alphadev.dictator.dao.item.Day;
import com.alphadev.dictator.dao.item.Session;
import com.alphadev.dictator.dao.item.Task;
import com.alphadev.dictator.properties.DesktopProperties;
import com.alphadev.dictator.properties.DictatorProperties;

import javafx.animation.AnimationTimer;
import javafx.animation.Interpolator;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.application.Platform;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ProgressIndicator;
import javafx.stage.Stage;
import javafx.util.Duration;

public class ControlerHeader implements Initializable, DataListener {
	static final Logger			LOG					= LogManager.getLogger(ControlerHeader.class);
	private static final int	ANIMATION_DURATION	= 250;

	@FXML
	private Button				plusMinusWindowAction;
	@FXML
	private DurationLabel		headerCurrentWorkingDayTime;
	@FXML
	private ProgressIndicator	refreshIndicator;
	@FXML
	private Button				refreshAction;

	private Stage				stage;
	private long				currentWorkingDayTimeCloseSession;
	private Session				currentSession;
	private int					bigHeight			= StageInitializer.BIG_HEIGHT;

	private DoubleProperty		stageHeight;

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		(new AnimationTimer() {
			@Override
			public void handle(long now) {
				headerCurrentWorkingDayTime.setDuration(currentWorkingDayTimeCloseSession + (currentSession == null ? 0 : currentSession.calcDuration()));
			}
		}).start();

		refreshIndicator.setVisible(false);

		DataProvider.addDataListener(this);
	}

	public void setStage(Stage stage) {
		this.stage = stage;

		stageHeight = new SimpleDoubleProperty(stage.getHeight());
		stageHeight.addListener((observable, oldValue, newValue) -> {
			if (newValue != null && newValue.doubleValue() != Double.NaN) {
				stage.setHeight(newValue.doubleValue());
			}
		});
	}

	@FXML
	public void onPlusMinusWindowAction() {
		boolean nextBig = stage.getHeight() == StageInitializer.SMALL_HEIGHT;
		double nextValue = nextBig ? this.bigHeight : StageInitializer.SMALL_HEIGHT;

		if (nextBig) {
			plusMinusWindowAction.getStyleClass().remove("plus");
			plusMinusWindowAction.getStyleClass().add("minus");
		}
		else {
			plusMinusWindowAction.getStyleClass().remove("minus");
			plusMinusWindowAction.getStyleClass().add("plus");
		}

		Timeline grow = new Timeline(new KeyFrame(Duration.millis(ANIMATION_DURATION), new KeyValue(stageHeight, nextValue, Interpolator.EASE_BOTH)));
		grow.setOnFinished(event -> {
			grow.jumpTo(Duration.ZERO);
		});

		grow.play();
	}

	@FXML
	public void onCloseWindowAction() {
		if ((boolean) DictatorProperties.get(DesktopProperties.CLOSE_WINDOWS_EXIT_APP)) {
			stage.getOnCloseRequest().handle(null);
		}
		else {
			stage.hide();
		}
	}

	@FXML
	public void onRefreshAction() {
		javafx.concurrent.Task<Void> syncSessions = new javafx.concurrent.Task<Void>() {
			@Override
			public Void call() {
				Backend.askSync();
				return null;
			}
		};

		refreshAction.setDisable(true);
		refreshIndicator.setVisible(true);
		syncSessions.setOnSucceeded(e -> {
			refreshAction.setDisable(false);
			refreshIndicator.setVisible(false);
		});
		new Thread(syncSessions).start();
	}

	@Override
	public void onSetCurrentSession(Session session) {
		currentSession = session;
	}

	@Override
	public void onSetTodayCloseSession(List<Session> todayCloseSession) {
		currentWorkingDayTimeCloseSession = 0;
		if (todayCloseSession != null) {
			for (Session session : todayCloseSession) {
				currentWorkingDayTimeCloseSession += session.calcDuration();
				// LOG.debug("close session : [{}]", session);
			}
		}
	}

	@Override
	public void onSetWeekSummary(Map<Day, List<Session>> weekSummary) {
		// Not used here
	}

	@Override
	public void onSetWorkingTasks(List<Task> tasks) {
		Platform.runLater(() -> {
			if (stage.getHeight() != StageInitializer.SMALL_HEIGHT) {
				this.bigHeight = StageInitializer.BIG_HEIGHT + tasks.size() * 45;
				stage.setHeight(this.bigHeight);
			}
		});
	}

	@Override
	public void onChangeSession(Map<String, Session> copySession) {

	}

	@Override
	public void onChangeTasks(Map<String, Task> copyTasks) {

	}
}
