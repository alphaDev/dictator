package com.alphadev.dictator.client.ui.component;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.time.DateUtils;

import com.alphadev.dictator.Constant;
import com.alphadev.dictator.client.ui.control.DateLabel;
import com.alphadev.dictator.client.ui.control.DurationLabel;
import com.alphadev.dictator.client.ui.part.InfoSessionModal;
import com.alphadev.dictator.dao.DataListener;
import com.alphadev.dictator.dao.DataProvider;
import com.alphadev.dictator.dao.item.Day;
import com.alphadev.dictator.dao.item.Session;
import com.alphadev.dictator.dao.item.Task;

import javafx.application.Platform;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;

public class ListSessions extends VBox implements DataListener {

	private InfoSessionModal infoSessionModal = new InfoSessionModal();

	public ListSessions() {
		super();
		DataProvider.addDataListener(this);
	}

	@Override
	public void onSetCurrentSession(Session session) {

	}

	@Override
	public void onSetWorkingTasks(List<Task> tasks) {

	}

	@Override
	public void onSetTodayCloseSession(List<Session> todayCloseSession) {

	}

	@Override
	public void onSetWeekSummary(Map<Day, List<Session>> weekSummary) {

	}

	@Override
	public void onChangeSession(Map<String, Session> sessions) {
		Platform.runLater(() -> updateSessions(sessions));
	}

	@Override
	public void onChangeTasks(Map<String, Task> copyTasks) {

	}

	private void updateSessions(Map<String, Session> copySession) {
		this.getChildren().clear();
		List<Session> sessions = new ArrayList<>(copySession.values());
		sessions.sort(Session::compare);

		int nbDayMax = 7;
		int nbDay = 0;
		int currentDay = -1;
		VBox dayContener = new VBox();
		dayContener.getStyleClass().add("day");
		HBox currentDateSeparator = null;
		Node lastSession = null;
		long dayDuration = 0;
		for (Session session : sessions) {
			Calendar startCalendar = DateUtils.toCalendar(session.getStart());
			int day = startCalendar.get(Calendar.DAY_OF_YEAR);
			if (currentDay != day) {
				currentDay = day;

				if (currentDateSeparator != null) {
					DurationLabel durationText = new DurationLabel(dayDuration);
					durationText.getStyleClass().add("duration");
					currentDateSeparator.getChildren().add(durationText);
					dayDuration = 0;

					this.getChildren().add(dayContener);

					dayContener = new VBox();
					dayContener.getStyleClass().add("day");
				}

				if (lastSession != null) {
					lastSession.getStyleClass().add("session-last");
				}

				nbDay++;
				if (nbDay > nbDayMax) {
					break;
				}
				currentDateSeparator = getDateSepartor(startCalendar);
				dayContener.getChildren().add(currentDateSeparator);
			}

			lastSession = getSessionNode(session);
			dayContener.getChildren().add(lastSession);
			dayDuration += session.calcDuration();
		}
		if (lastSession != null) {
			lastSession.getStyleClass().add("sessionNodeLast");
		}
	}

	private Node getSessionNode(Session session) {
		BorderPane line = new BorderPane();
		line.getStyleClass().add("session-line");

		HBox right = new HBox(2);
		HBox left = new HBox(2);
		left.setAlignment(Pos.CENTER_LEFT);

		if (session.task() == null) {
			Text sessionDur = new Text(Constant.NO_PROJET_DESC);
			left.getChildren().add(sessionDur);
		}
		else {
			left.getChildren().add(new TaskColor(session.task()));

			Text taskKey = new Text(session.task().getKey());
			left.getChildren().add(taskKey);
		}

		Text sessionDesc = new Text(session.getDescription());
		left.getChildren().add(sessionDesc);

		if (session.running()) {
			Label sessionDur = new Label("running");
			right.getChildren().add(sessionDur);
		}
		else {
			DurationLabel sessionDur = new DurationLabel(session.calcDuration());
			right.getChildren().add(sessionDur);
		}

		line.setRight(right);
		line.setLeft(left);
		line.setOnMousePressed((e) -> {
			infoSessionModal.show(session);
		});
		return line;
	}

	private HBox getDateSepartor(Calendar startCalendar) {
		HBox line = new HBox(2);
		line.getStyleClass().add("date");
		DateLabel date = new DateLabel(startCalendar.getTime());
		line.getChildren().add(date);
		return line;
	}

}
