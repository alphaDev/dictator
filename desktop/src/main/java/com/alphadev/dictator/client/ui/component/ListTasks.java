package com.alphadev.dictator.client.ui.component;

import java.io.IOException;
import java.net.URL;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.alphadev.dictator.DateFormater;
import com.alphadev.dictator.client.ui.StageInitializer;
import com.alphadev.dictator.client.ui.control.DurationLabel;
import com.alphadev.dictator.client.ui.part.InfoTaskModal;
import com.alphadev.dictator.dao.DataListener;
import com.alphadev.dictator.dao.DataProvider;
import com.alphadev.dictator.dao.PluginsManager;
import com.alphadev.dictator.dao.item.Day;
import com.alphadev.dictator.dao.item.Session;
import com.alphadev.dictator.dao.item.Task;

import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Region;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;

public class ListTasks extends VBox implements Initializable, DataListener {

	static final Logger			LOG				= LogManager.getLogger(ListTasks.class);

	@FXML
	private ListView<Task>		listTask;
	@FXML
	private TextField			taskFilter;

	private InfoTaskModal		infoTaskModal	= new InfoTaskModal();
	private Map<Task, Region>	taskLines		= new HashMap<>();
	private FilteredList<Task>	items;

	public ListTasks() {
		super();
		FXMLLoader fxmlLoader = new FXMLLoader(StageInitializer.CLASS_LOADER.getResource(StageInitializer.FXML_LIST_TASKS));
		this.getStylesheets().add(StageInitializer.getStylesheet());
		fxmlLoader.setRoot(this);
		fxmlLoader.setController(this);
		try {
			fxmlLoader.load();
		}
		catch (IOException exception) {
			throw new RuntimeException(exception);
		}
		DataProvider.addDataListener(this);
	}

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		listTask.setCellFactory(param -> new TaskListCell());
		taskFilter.textProperty().addListener(e -> {
			if (items != null) {
				items.setPredicate(this::filterListPredicate);
			}
		});

	}

	@Override
	public void onSetCurrentSession(Session session) {

	}

	@Override
	public void onSetWorkingTasks(List<Task> tasks) {

	}

	@Override
	public void onSetTodayCloseSession(List<Session> todayCloseSession) {

	}

	@Override
	public void onSetWeekSummary(Map<Day, List<Session>> weekSummary) {

	}

	@Override
	public void onChangeSession(Map<String, Session> sessions) {

	}

	@Override
	public void onChangeTasks(Map<String, Task> tasks) {
		Platform.runLater(() -> updateTasks(tasks.values()));
	}

	private void updateTasks(Collection<Task> values) {
		final ObservableList<Task> observableArrayList = FXCollections.observableArrayList(values);
		observableArrayList.sort(Task::compareByLastSession);
		items = new FilteredList<>(observableArrayList, this::filterListPredicate);

//		LOG.debug("updateTasks -> [{}]", items.size());
		taskLines.clear();
		listTask.setItems(items);
	}

	private boolean filterListPredicate(Task task) {
		String filterText = taskFilter.getText();
		if (StringUtils.isEmpty(filterText)) {
			return true;
		}

		if (StringUtils.contains(task.getKey(), filterText)) {
			return true;
		}
		if (StringUtils.contains(task.getDescription(), filterText)) {
			return true;
		}

		return false;
	}

	private BorderPane makeTaskLine(Task task) {
		BorderPane line = new BorderPane();
		line.getStyleClass().add("task-line");
		line.setMinWidth(0);
		line.setPrefWidth(1);
		HBox left = new HBox();
		left.getStyleClass().add("hbox");
		HBox right = new HBox();
		right.getStyleClass().add("hbox");

		final TaskColor taskColor = new TaskColor(task);
		taskColor.setRadius(TaskColor.RADIUS);
		left.getChildren().add(taskColor);
		Text taskKey = new Text(task.getKey());
		left.getChildren().add(taskKey);
		left.getChildren().add(new Label(task.getDescription()));
		DurationLabel timeSpend = new DurationLabel(task.calcTimeSpend());
		right.getChildren().add(timeSpend);
		Button startSession = new Button();
		startSession.getStyleClass().add("play");
		startSession.setOnAction(e -> {
			Date now = Calendar.getInstance().getTime();
			LOG.debug("createSession : [{}] at [{}]", task.getKey(), DateFormater.format(now));
			PluginsManager.createSession(task, now, null, "");
		});
		right.getChildren().add(startSession);
		line.setCenter(left);
		line.setRight(right);

		line.setOnMouseClicked((e) -> infoTaskModal.show(task));
		return line;
	}

	private class TaskListCell extends ListCell<Task> {

		@Override
		protected void updateItem(Task task, boolean empty) {
			super.updateItem(task, empty);

			if (empty || task == null) {
				setGraphic(null);
				setText(null);
			}
			else {
				Region line = taskLines.getOrDefault(task, null);
				if (line == null) {
					taskLines.put(task, makeTaskLine(task));
				}
				setGraphic(taskLines.get(task));
			}
		}
	}

}
