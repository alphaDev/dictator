package com.alphadev.dictator.client.ui.part;

import java.net.URL;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.alphadev.dictator.Constant;
import com.alphadev.dictator.DateFormater;
import com.alphadev.dictator.client.ui.StageInitializer;
import com.alphadev.dictator.client.ui.component.TaskColor;
import com.alphadev.dictator.client.ui.control.DateLabel;
import com.alphadev.dictator.client.ui.control.DurationLabel;
import com.alphadev.dictator.dao.DataListener;
import com.alphadev.dictator.dao.DataProvider;
import com.alphadev.dictator.dao.PluginsManager;
import com.alphadev.dictator.dao.item.Day;
import com.alphadev.dictator.dao.item.Session;
import com.alphadev.dictator.dao.item.Task;
import com.alphadev.dictator.dao.item.property.PropertyInfo;
import com.alphadev.dictator.plugin.PluginInfo;

import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.util.Callback;
import javafx.util.StringConverter;

public class InfoSessionModal extends BorderModal implements Initializable, DataListener {
	private static final int	MODAL_WIDTH	= 300;

	static final Logger			LOG			= LogManager.getLogger(InfoSessionModal.class);

	@FXML
	private HBox				fromPlugin;
	@FXML
	private VBox				customs;
	@FXML
	private TextField			description;
	@FXML
	private DatePicker			date;
	@FXML
	private TextField			startTime;
	@FXML
	private TextField			endTime;
	@FXML
	private ComboBox<Task>		listTasks;
	@FXML
	private DurationLabel		duration;

	private boolean				customReady	= false;
	private Session				session;

	public InfoSessionModal() {
		super(StageInitializer.FXML_PART_INFO_SESSION, MODAL_WIDTH);
		DataProvider.addDataListener(this);
	}

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		Callback<ListView<Task>, ListCell<Task>> cellFactory = new Callback<ListView<Task>, ListCell<Task>>() {
			@Override
			public ListCell<Task> call(ListView<Task> l) {
				return new ListCell<Task>() {
					@Override
					protected void updateItem(Task item, boolean empty) {
						super.updateItem(item, empty);
						String taskKey = Constant.NO_PROJET_KEY;

						if (item != null && !empty && item.getKey() != null) {
							taskKey = item.getKey();
						}

						HBox tagPane = new HBox(2.0);
						tagPane.setAlignment(Pos.CENTER_LEFT);
						Label name = new Label(taskKey);
						name.setFont(new Font(name.getFont().getName(), 14));
						name.setTextFill(Color.web("#000000"));
						tagPane.getChildren().add(new TaskColor(item));
						tagPane.getChildren().add(name);
						setGraphic(tagPane);
					}
				};
			}
		};

		listTasks.setButtonCell((ListCell<Task>) cellFactory.call(null));
		listTasks.setCellFactory(cellFactory);
		listTasks.setPrefWidth(-1);

		date.setShowWeekNumbers(false);
		date.setConverter(new StringConverter<LocalDate>() {
			DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern(DateLabel.getFormat());

			@Override
			public String toString(LocalDate date) {
				if (date != null) {
					return dateFormatter.format(date);
				}
				else {
					return "";
				}
			}

			@Override
			public LocalDate fromString(String string) {
				if (string != null && !string.isEmpty()) {
					return LocalDate.parse(string, dateFormatter);
				}
				else {
					return null;
				}
			}
		});

	}

	public void show(Session session) {
		setSession(session);
		super.show();
	}

	@FXML
	public void onClose() {
		super.hide();
	}

	@FXML
	public void onSave() {
		this.session.setDescription(description.getText());
		if (listTasks.getValue() != null) {
			this.session.setTaskKey(listTasks.getValue().getKey());
		}
		else {
			this.session.setTaskKey(null);
		}
		LocalDate date = this.date.getValue();
		String startTimeString = this.startTime.getText();
		String endTimeString = this.endTime.getText();
		if (date == null) {
			this.session.setStop(null);
			this.session.setStart(null);
		}
		else {
			if (endTimeString == null) {
				this.session.setStop(null);
			}
			else {
				try {
					int hour = Integer.parseInt(endTimeString.split(":")[0]);
					int minute = Integer.parseInt(endTimeString.split(":")[1]);
					Date stop = Date.from(date.atTime(hour, minute).atZone(ZoneId.systemDefault()).toInstant());
					this.session.setStop(stop);
				}
				catch (Exception e) {
					LOG.error("Bad time format for end time. Expect HH:mm and get [{}]", endTimeString);
				}

			}

			if (startTimeString == null) {
				this.session.setStop(null);
			}
			else {
				try {
					int hour = Integer.parseInt(startTimeString.split(":")[0]);
					int minute = Integer.parseInt(startTimeString.split(":")[1]);
					Date start = Date.from(date.atTime(hour, minute).atZone(ZoneId.systemDefault()).toInstant());
					this.session.setStart(start);
				}
				catch (Exception e) {
					LOG.error("Bad time format for end time. Expect HH:mm and get [{}]", endTimeString);
				}

			}
		}

		PluginsManager.updateSession(session);
	}

	@FXML
	public void onDelete() {
		LOG.debug("deleteSession [{}]", session);
		PluginsManager.deleteSession(session);
		super.hide();
	}

	private void setSession(Session session) {
		this.session = session;

		description.setText(null);
		date.setValue(null);
		startTime.setText(null);
		endTime.setText(null);
		listTasks.setValue(null);

		for (PropertyInfo custom : Session.getCustomProp()) {
			custom.resetControl();
		}

		if (session != null) {
			if (this.session.getDescription() != null) {
				description.setText(this.session.getDescription());
			}
			if (this.session.getStart() != null) {
				startTime.setText(DateFormater.format("HH:mm", this.session.getStart()));
			}
			if (this.session.getStart() != null) {
				Date input = this.session.getStart();
				LocalDate localDate = input.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
				date.setValue(localDate);
			}
			if (this.session.getStop() != null) {
				endTime.setText(DateFormater.format("HH:mm", this.session.getStop()));
			}
			if (this.session.task() != null) {
				listTasks.setValue(this.session.task());
			}

			fromPlugin.getChildren().clear();
			for (PluginInfo pluginInfo : session.getFromPlugin()) {
				final BorderPane logo = new BorderPane(pluginInfo.getLogo());
				logo.getStyleClass().add("plugin-logo");
				fromPlugin.getChildren().add(logo);
			}

			if (session.running()) {
				duration.setText("running");
			}
			else {
				duration.setDuration(session.calcDuration());
			}

			for (PropertyInfo custom : Session.getCustomProp()) {
				custom.setControlValue(session.getCustomValue(custom.getName(), custom.getType().getTypeClass()));
			}

		}
	}

	private void prepareCustom() {
		if (customReady) {
			return;
		}
		List<PropertyInfo> sessionCustoms = new ArrayList<PropertyInfo>(Session.getCustomProp());
		if (!sessionCustoms.isEmpty()) {
			sessionCustoms.sort((p1, p2) -> {
				return p1.getFrom().getName().compareTo(p2.getFrom().getName());
			});

			GridPane grid = new GridPane();
			grid.getStyleClass().add("custom-grid");
			PluginInfo currentPlugin = sessionCustoms.get(0).getFrom();
			int rowIndex = 0;
			grid.add(currentPlugin.getLogo(), 0, rowIndex, 2, 1);

			for (PropertyInfo propertyInfo : sessionCustoms) {
				rowIndex++;
				if (!currentPlugin.equals(propertyInfo.getFrom())) {
					grid.add(currentPlugin.getLogo(), 0, rowIndex, 2, 1);
					rowIndex++;
				}

				switch (propertyInfo.getType()) {
					case STRING:
					default:
						grid.add(new Label(propertyInfo.getName()), 0, rowIndex);
						grid.add(propertyInfo.getControl(), 1, rowIndex);
					break;
				}
			}

			customs.getChildren().add(grid);
		}

		customReady = true;
	}

	@Override
	public void onSetCurrentSession(Session session) {
	}

	@Override
	public void onSetWorkingTasks(List<Task> tasks) {
	}

	@Override
	public void onSetTodayCloseSession(List<Session> todayCloseSession) {
	}

	@Override
	public void onSetWeekSummary(Map<Day, List<Session>> weekSummary) {
	}

	@Override
	public void onChangeSession(Map<String, Session> sessions) {
		Platform.runLater(this::prepareCustom);
	}

	@Override
	public void onChangeTasks(Map<String, Task> tasks) {
		Platform.runLater(() -> {
			if (listTasks.getItems().isEmpty()) {
				listTasks.setOnAction(null);
				listTasks.getItems().clear();
				listTasks.getItems().addAll(tasks.values());
			}
		});

	}
}
