package com.alphadev.dictator.client;

import java.text.ParseException;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.alphadev.dictator.DurationFormater;
import com.alphadev.dictator.client.idle.IdleTimeDetector;
import com.alphadev.dictator.client.idle.IdleTimeDetectorFactory;
import com.alphadev.dictator.dao.DataProvider;
import com.alphadev.dictator.dao.PluginsManager;
import com.alphadev.dictator.dao.item.Day;
import com.alphadev.dictator.dao.item.Session;
import com.alphadev.dictator.dao.item.Task;
import com.alphadev.dictator.properties.CommonProperties;
import com.alphadev.dictator.properties.DesktopProperties;
import com.alphadev.dictator.properties.DictatorProperties;

public class Backend {

	private static final Logger	LOG					= LogManager.getLogger(Backend.class);

	private static Backend		instance			= new Backend();
	private IdleTimeDetector	idleTimeDetector	= IdleTimeDetectorFactory.newIdleTimeDetector();
	private long				idleStartMillis		= -1;

	private Backend() {

	}

	public static void start() {
		instance.init();
	}

	public static void askSync() {
		LOG.info("full sync start");
		PluginsManager.syncTasks(true);
		PluginsManager.syncSessions(true);
	}

	private void init() {
		LOG.info("Backend init");

		DataProvider.init();
		PluginsManager.loadPlugins();
		PluginsManager.askPropertiesNeed();
		PluginsManager.initPlugins();

		DictatorProperties.reload();
		DurationFormater.setWorkingDayDuration(DictatorProperties.get(CommonProperties.WORKING_HOURS));

		PluginsManager.askCustomProperties();
		PluginsManager.setUser();
		PluginsManager.updateUser();

		if (!DataProvider.isHasRestoreData()) {
			askSync();
		}

		new Timer(true).scheduleAtFixedRate(new SyncLoop(), 5000l, 10000l);
		new Timer(true).scheduleAtFixedRate(new IdleLoop(), 5000l, 500l);

		LOG.info("Backend init done");
	}

	private class IdleLoop extends TimerTask {
		@Override
		public void run() {
			long idleTimeMillis = idleTimeDetector.getIdleTimeMillis();
			long idleAcceptTime = DictatorProperties.get(DesktopProperties.IDLE_ACCEPT_TIME);
			if (idleStartMillis < 0 && idleTimeMillis > idleAcceptTime) {
				idleStartMillis = System.currentTimeMillis() - idleTimeMillis;
				LOG.debug("idle event [{}]", idleStartMillis);
			}
			if (idleStartMillis > 0 && idleTimeMillis < idleAcceptTime) {
				long wakeTime = System.currentTimeMillis();
				LOG.debug("wake event [{}]", wakeTime);
				idleStartMillis = -1;
			}
		}
	}

	private class SyncLoop extends TimerTask {

		@Override
		public void run() {
			LOG.info("small sync start");
			PluginsManager.syncTasks(false);
			PluginsManager.syncSessions(false);
		}

	}

	public static void main(String[] args) throws ParseException {
		Backend.start();

		LOG.info("--------------------------------");
		LOG.info("tasks load");
		for (Task task : DataProvider.getTasks()) {
			LOG.info("task [{}]", task);
			for (Session session : task.sessions()) {
				LOG.info("\tsession [{}]", session);
			}
		}

		Session curSession = DataProvider.getCurrentSession();
		if (curSession != null) {
			LOG.info("--------------------------------");
			LOG.info("current session");
			LOG.info("\tsession [{}]", curSession);
			LOG.info("\ton task [{}]", curSession.task());
		}

		LOG.info("--------------------------------");
		LOG.info("current week summary");
		// SimpleDateFormat sdf = new SimpleDateFormat("dd-M-yyyy");
		// String dateInString = "30-04-2017";
		// Date date = sdf.parse(dateInString);
		// final Map<Day, List<Session>> weekSummary = DataProvider.getWeekSummary(date);
		final Map<Day, List<Session>> weekSummary = DataProvider.getCurrentWeekSummary();
		for (Day d : Day.values()) {
			StringBuilder sb = new StringBuilder();
			boolean first = true;
			for (Session session : weekSummary.get(d)) {
				if (first) {
					first = false;
				}
				else {
					sb.append(" - ");
				}
				sb.append(session);
			}

			LOG.info("day [{}] - [{}]", d, sb);
		}

	}
}
