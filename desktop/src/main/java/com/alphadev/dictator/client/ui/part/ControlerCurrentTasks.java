package com.alphadev.dictator.client.ui.part;

import java.net.URL;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.alphadev.dictator.client.ui.component.LineTask;
import com.alphadev.dictator.dao.DataListener;
import com.alphadev.dictator.dao.DataProvider;
import com.alphadev.dictator.dao.item.Day;
import com.alphadev.dictator.dao.item.Session;
import com.alphadev.dictator.dao.item.Task;

import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;

public class ControlerCurrentTasks implements Initializable, DataListener {
	private static final Logger	LOG	= LogManager.getLogger(ControlerCurrentTasks.class);

	@FXML
	private Label				noWorkingTasksLabel;
	@FXML
	private Label				workingTasksLabel;
	@FXML
	private VBox				workingTasks;

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		DataProvider.addDataListener(this);
	}

	@Override
	public void onSetCurrentSession(Session session) {

	}

	@Override
	public void onSetTodayCloseSession(List<Session> todayCloseSession) {

	}

	@Override
	public void onSetWeekSummary(Map<Day, List<Session>> weekSummary) {

	}

	@Override
	public void onSetWorkingTasks(List<Task> tasks) {
		tasks.stream().forEach(t -> LOG.debug("working task [{}]", t));
		Platform.runLater(() -> setWorkingTasks(tasks));

	}

	@Override
	public void onChangeSession(Map<String, Session> copySession) {

	}

	@Override
	public void onChangeTasks(Map<String, Task> copyTasks) {

	}

	private void setWorkingTasks(List<Task> tasks) {
		workingTasks.getChildren().clear();
		if (tasks.isEmpty()) {
			workingTasks.getChildren().add(noWorkingTasksLabel);
		}
		else {
			workingTasks.getChildren().add(workingTasksLabel);
			for (Task task : tasks) {
				workingTasks.getChildren().add(new LineTask(task));
			}
		}
	}
}
