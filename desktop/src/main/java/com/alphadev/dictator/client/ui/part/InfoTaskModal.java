package com.alphadev.dictator.client.ui.part;

import java.net.URL;
import java.util.ResourceBundle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.alphadev.dictator.client.ui.StageInitializer;
import com.alphadev.dictator.client.ui.component.TaskColor;
import com.alphadev.dictator.dao.item.Task;
import com.alphadev.dictator.plugin.PluginInfo;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;

public class InfoTaskModal extends BorderModal implements Initializable {
	private static final int	MODAL_WIDTH	= 300;

	static final Logger			LOG			= LogManager.getLogger(InfoTaskModal.class);

	@FXML
	private HBox				fromPlugin;
	@FXML
	private TaskColor			taskColor;
	@FXML
	private Label				taskKey;
	@FXML
	private Label				taskDescription;

	// private Task task;

	public InfoTaskModal() {
		super(StageInitializer.FXML_PART_INFO_TASK, MODAL_WIDTH);
	}

	@Override
	public void initialize(URL location, ResourceBundle resources) {

	}

	public void show(Task task) {
		setTask(task);
		super.show();
	}

	@FXML
	public void onClose() {
		super.hide();
	}

	@FXML
	public void onDelete() {

	}

	private void setTask(Task task) {
		// this.task = task;

		taskColor.setTask(task);
		taskKey.setText(task.getKey());
		taskDescription.setText(task.getDescription());

		fromPlugin.getChildren().clear();
		for (PluginInfo pluginInfo : task.getFromPlugin()) {
			final BorderPane logo = new BorderPane(pluginInfo.getLogo());
			logo.getStyleClass().add("plugin-logo");
			fromPlugin.getChildren().add(logo);
		}
	}

}
