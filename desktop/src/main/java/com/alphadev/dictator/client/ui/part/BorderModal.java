package com.alphadev.dictator.client.ui.part;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.alphadev.dictator.client.ui.StageInitializer;
import com.alphadev.dictator.exception.DictatorRuntimeException;

import javafx.animation.Interpolator;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Duration;

public class BorderModal {
	static final Logger					LOG					= LogManager.getLogger(BorderModal.class);
	private static final int			ANIMATION_DURATION	= 250;

	private static List<BorderModal>	allModals			= new ArrayList<>();
	private boolean						haveResize;
	private int							width;
	private Stage						dialog;
	private DoubleProperty				dialogX;
	private Timeline					hiding;
	private Timeline					showing;

	public BorderModal(String fxml, int width) {
		FXMLLoader fxmlLoader = new FXMLLoader(StageInitializer.CLASS_LOADER.getResource(fxml));
		fxmlLoader.setController(this);
		Parent root;
		try {
			root = fxmlLoader.load();
		}
		catch (IOException e) {
			throw new DictatorRuntimeException(e);
		}

		init(root, width);
	}

	public BorderModal(Parent root, int width) {
		init(root, width);
	}

	private void init(Parent root, int width) {
		dialog = new Stage();
		Scene scene = new Scene(root);
		scene.getStylesheets().add(StageInitializer.getStylesheet());
		scene.setFill(Color.TRANSPARENT);
		this.width = width;

		dialog.setScene(scene);
		dialog.initOwner(StageInitializer.getStage());
		dialog.initStyle(StageStyle.TRANSPARENT);
		dialog.setAlwaysOnTop(true);

		dialogX = new SimpleDoubleProperty();
		dialogX.addListener((observable, oldValue, newValue) -> {
			if (newValue != null && newValue.doubleValue() != Double.NaN) {
				dialog.setX(newValue.doubleValue());
			}
		});

		posModal();
		StageInitializer.getStage().xProperty().addListener(this::stageChangePos);
		StageInitializer.getStage().yProperty().addListener(this::stageChangePos);
		StageInitializer.getStage().heightProperty().addListener(this::stageChangeSize);

		allModals.add(this);
	}

	public void hide() {
		hiding.play();
		dialog.setAlwaysOnTop(false);
	}

	public void show() {
		if (!dialog.isShowing()) {
			posModal();
			dialog.show();
			dialog.setAlwaysOnTop(false);
			if (!haveResize) {
				dialog.sizeToScene();
				dialog.setWidth(width);
				posModal();
				haveResize = true;
			}
			showing.play();

			allModals.stream().filter((m) -> !m.equals(this)).forEach((m) -> m.dialog.hide());
		}
	}

	public void stageChangePos(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
		posModal();
	}

	public void stageChangeSize(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
		dialog.hide();
	}

	private void posModal() {
		double posXShow = StageInitializer.getStage().getX() - dialog.getWidth();
		double posXHide = StageInitializer.getStage().getX();
		if (posXShow < 0) {
			posXShow = StageInitializer.getStage().getX() + StageInitializer.getStage().getWidth();
			posXHide = posXShow - dialog.getWidth();
		}

		if (dialog.isShowing()) {
			dialogX.setValue(posXShow);
		}
		else {
			dialogX.setValue(posXHide);
		}

		if (hiding != null && hiding.getCurrentTime() != null && hiding.getCurrentTime().toMillis() != 0) {
			hiding.jumpTo(Duration.millis(ANIMATION_DURATION));
		}
		if (showing != null && showing.getCurrentTime() != null && showing.getCurrentTime().toMillis() != 0) {
			showing.jumpTo(Duration.millis(ANIMATION_DURATION));
		}

		showing = new Timeline(new KeyFrame(Duration.millis(ANIMATION_DURATION), new KeyValue(dialogX, posXShow, Interpolator.EASE_BOTH)));
		showing.setOnFinished(event -> {
			showing.jumpTo(Duration.ZERO);
			dialog.setAlwaysOnTop(true);
		});

		hiding = new Timeline(new KeyFrame(Duration.millis(ANIMATION_DURATION), new KeyValue(dialogX, posXHide, Interpolator.EASE_BOTH)));
		hiding.setOnFinished(event -> {
			hiding.jumpTo(Duration.ZERO);
			dialog.hide();
			dialog.setAlwaysOnTop(true);
		});

		dialog.setY(StageInitializer.getStage().getY() + (StageInitializer.getStage().getHeight() - dialog.getHeight() - 20));

	}
}
