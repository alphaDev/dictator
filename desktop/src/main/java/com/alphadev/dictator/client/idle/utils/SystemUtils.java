package com.alphadev.dictator.client.idle.utils;

import java.util.Hashtable;
import java.util.Map;

/**
 * Collection of utility methods used as a convenience for retrieving JVM-level or System-level information.
 *
 * @author Laurent Cohen
 */
public final class SystemUtils {

	/**
	 * The MAC based OS.
	 */
	private static final int			MAC_OS			= 3;
	/**
	 * The type of this host's OS.
	 */
	private static final int			OS_TYPE			= determineOSType();
	/**
	 * Holds and manages the shutdown hooks set on the JVM.
	 */
	private static Map<String, Thread>	shutdownHooks	= new Hashtable<>();
	/**
	 * Unknown or unsupported OS.
	 */
	private static final int			UNKNOWN_OS		= -1;
	/**
	 * Windows OS.
	 */
	private static final int			WINDOWS_OS		= 1;
	/**
	 * X11 based OS.
	 */
	private static final int			X11_OS			= 2;

	/**
	 * Add the specified shutdown hook with the specified key.
	 *
	 * @param key
	 *            the hokk's key.
	 * @param hook
	 *            the shutdown hook to add.
	 */
	public static void addShutdownHook(final String key, final Thread hook) {
		shutdownHooks.put(key, hook);
		Runtime.getRuntime().addShutdownHook(hook);
	}

	/**
	 * Determine the type of this host's operating system, based on the value of the system property &quot;os.name&quot;.
	 *
	 * @return an int value identifying the type of OS.
	 */
	private static int determineOSType() {
		String name = System.getProperty("os.name");
		if (startsWithOneOf(name, true, "Linux", "SunOS", "Solaris", "FreeBSD", "OpenBSD")) return X11_OS;
		else if (startsWithOneOf(name, true, "Mac", "Darwin")) return MAC_OS;
		else if (name.startsWith("Windows") && !name.startsWith("Windows CE")) return WINDOWS_OS;
		return UNKNOWN_OS;
	}

	/**
	 * Determine whether the current OS is Mac-based.
	 *
	 * @return true if the system is Mac-based, false otherwise.
	 */
	public static boolean isMac() {
		return OS_TYPE == MAC_OS;
	}

	/**
	 * Determine whether the current OS is Windows.
	 *
	 * @return true if the system is Windows, false otherwise.
	 */
	public static boolean isWindows() {
		return OS_TYPE == WINDOWS_OS;
	}

	/**
	 * Determine whether the current OS is X11-based.
	 *
	 * @return true if the system is X11, false otherwise.
	 */
	public static boolean isX11() {
		return OS_TYPE == X11_OS;
	}

	private static boolean startsWithOneOf(final String source, final boolean ignoreCase, final String... values) {
		if ((source == null) || (values == null)) return false;
		String s = ignoreCase ? source.toLowerCase() : source;
		for (String val : values) {
			if (val == null) continue;
			String s2 = ignoreCase ? val.toLowerCase() : val;
			if (s.startsWith(s2)) return true;
		}
		return false;
	}

	/**
	 * Instantiation of this class is not permitted.
	 */
	private SystemUtils() {
	}
}
