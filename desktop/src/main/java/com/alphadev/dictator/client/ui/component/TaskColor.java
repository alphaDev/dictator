package com.alphadev.dictator.client.ui.component;

import org.apache.commons.lang3.StringUtils;

import com.alphadev.dictator.dao.item.Task;

import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;

public class TaskColor extends Circle {
	public static final int RADIUS = 6;

	public TaskColor() {
		setTask(null);
		this.getStyleClass().add("task-color");
	}

	public TaskColor(Task task) {
		setTask(task);
		this.getStyleClass().add("task-color");
	}

	public void setTask(Task task) {
		if (task == null || StringUtils.isBlank(task.getColor())) {
			this.setRadius(0);
			this.setVisible(false);
		}
		else {
			this.setRadius(RADIUS);
			this.setVisible(true);
			this.setFill(Color.web(task.getColor()));
		}

	}

}
