package com.alphadev.dictator.client.ui.control;

import java.util.Objects;

import javafx.beans.InvalidationListener;
import javafx.beans.Observable;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.chart.Axis;
import javafx.scene.chart.StackedBarChart;
import javafx.scene.shape.Line;

public class StackedBarChartWithMarker<X, Y> extends StackedBarChart<X, Y> {

	public StackedBarChartWithMarker(Axis<X> xAxis, Axis<Y> yAxis) {
		super(xAxis, yAxis);
		// a list that notifies on change of the yValue property
		horizontalMarkers = FXCollections.observableArrayList(d -> new Observable[] { d.YValueProperty() });
		// listen to list changes and re-plot
		horizontalMarkers.addListener((InvalidationListener) observable -> layoutPlotChildren());
		
		this.getStyleClass().add("stacked-bar-chart-with-marker");
	}

	// data defining horizontal markers, xValues are ignored
	private ObservableList<Data<X, Y>> horizontalMarkers;

	public void clearAllData() {
		horizontalMarkers.clear();
		getData().clear();
	}

	/**
	 * Add horizontal value marker. The marker's Y value is used to plot a horizontal line across the plot area, its X value is ignored.
	 *
	 * @param marker
	 *            must not be null.
	 */
	public void addHorizontalValueMarker(Data<X, Y> marker, String color) {
		Objects.requireNonNull(marker, "the marker must not be null");
		if (horizontalMarkers.contains(marker)) return;
		Line line = new Line();
		line.setStyle("-fx-stroke: " + color + ";");
		marker.setNode(line);
		getPlotChildren().add(line);
		horizontalMarkers.add(marker);
	}

	/**
	 * Remove horizontal value marker.
	 *
	 * @param horizontalMarker
	 *            must not be null
	 */
	public void removeHorizontalValueMarker(Data<X, Y> marker) {
		Objects.requireNonNull(marker, "the marker must not be null");
		if (marker.getNode() != null) {
			getPlotChildren().remove(marker.getNode());
			marker.setNode(null);
		}
		horizontalMarkers.remove(marker);
	}

	/**
	 * Overridden to layout the value markers.
	 */
	@Override
	protected void layoutPlotChildren() {
		super.layoutPlotChildren();
		for (Data<X, Y> horizontalMarker : horizontalMarkers) {
			Line line = (Line) horizontalMarker.getNode();
			line.setStartX(0);
			line.setEndX(getXAxis().getLayoutX() + getXAxis().getWidth());
			line.setStartY(getYAxis().getDisplayPosition(horizontalMarker.getYValue()));
			line.setEndY(line.getStartY());

		}
	}
}
