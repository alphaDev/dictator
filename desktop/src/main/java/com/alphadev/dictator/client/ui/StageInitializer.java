package com.alphadev.dictator.client.ui;

import java.awt.GraphicsEnvironment;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.Toolkit;
import java.io.IOException;
import java.lang.reflect.Field;

import javax.imageio.ImageIO;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.alphadev.dictator.Constant;
import com.alphadev.dictator.client.Backend;
import com.alphadev.dictator.client.ui.control.DateLabel;
import com.alphadev.dictator.client.ui.part.ControlerHeader;
import com.alphadev.dictator.properties.DesktopProperties;
import com.alphadev.dictator.properties.DictatorProperties;

import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.application.Platform;
import javafx.beans.value.ObservableValue;
import javafx.concurrent.Task;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.TabPane;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Duration;

public class StageInitializer {
	public static final int				TOOLTIP_START_TIME		= 10;

	public static final String			MAIN_ICON				= "com/alphadev/dictator/client/ui/img/main_big.png";
	public static final String			SYS_TRAY_ICON			= "com/alphadev/dictator/client/ui/img/main.png";

	public static final int				BIG_HEIGHT				= 260;
	public static final int				SMALL_HEIGHT			= 78;

	public static final String			STYLESHEET_DEFAULT		= "com/alphadev/dictator/client/ui/mainUI.css";
	public static final String			STYLESHEET_NEON			= "com/alphadev/dictator/client/ui/mainUI-neon.css";

	public static final String			SKIN_NEON				= "neon";

	public static final String			FXML_MAIN				= "com/alphadev/dictator/client/ui/mainUI.fxml";
	public static final String			FXML_PART_HEADER		= "com/alphadev/dictator/client/ui/part/header.fxml";
	public static final String			FXML_PART_CURRENT_TASKS	= "com/alphadev/dictator/client/ui/part/currentTasks.fxml";
	public static final String			FXML_PART_SUB_PART		= "com/alphadev/dictator/client/ui/part/subPart.fxml";
	public static final String			FXML_PART_INFO_SESSION	= "com/alphadev/dictator/client/ui/part/infoSessionModal.fxml";
	public static final String			FXML_PART_INFO_TASK		= "com/alphadev/dictator/client/ui/part/infoTaskModal.fxml";
	public static final String			FXML_CURRENT_LINE_TASK	= "com/alphadev/dictator/client/ui/component/lineTask.fxml";
	public static final String			FXML_LIST_TASKS			= "com/alphadev/dictator/client/ui/component/listTasks.fxml";
	public static final int				INITAL_WIDTH			= 400;
	public static final int				MARGING_FROM_BORDER		= 10;
	public static final Logger			LOG						= LogManager.getLogger(StageInitializer.class);
	public static final ClassLoader		CLASS_LOADER			= StageInitializer.class.getClassLoader();

	private static double				xOffset;
	private static double				yOffset;
	private static Stage				stage;
	private static java.awt.TrayIcon	trayIcon;
	private static String				stylesheet;
	// private static Alert alert;

	public static void init(Stage stage) throws IOException {
		hackTooltipStartTiming(TOOLTIP_START_TIME);

		Task<Void> initBackendTask = new Task<Void>() {
			@Override
			public Void call() {
				Backend.start();
				return null;
			}
		};

		Font.loadFont(CLASS_LOADER.getResource("LCDMB.TTF").toExternalForm(), 10);

		String skin = DictatorProperties.get(DesktopProperties.SKIN);

		switch (skin) {
			case SKIN_NEON:
				stylesheet = STYLESHEET_NEON;
			break;
			default:
				stylesheet = STYLESHEET_DEFAULT;
		}

		String dateFormat = DictatorProperties.get(DesktopProperties.DATE_FORMAT);
		if (StringUtils.isNotEmpty(dateFormat)) {
			DateLabel.setFormat(dateFormat);
		}

		Rectangle screenBounds = getScreenBounds();
		Platform.setImplicitExit(false);
		StageInitializer.stage = stage;
		stage.setWidth(INITAL_WIDTH);
		stage.setHeight(BIG_HEIGHT);
		stage.setX(screenBounds.width - MARGING_FROM_BORDER - stage.getWidth());
		stage.setY(MARGING_FROM_BORDER);
		stage.setAlwaysOnTop(true);
		stage.initStyle(StageStyle.TRANSPARENT);
		stage.getIcons().add(new Image(CLASS_LOADER.getResourceAsStream(MAIN_ICON)));
		stage.setOnCloseRequest(event -> {
			java.awt.SystemTray tray = java.awt.SystemTray.getSystemTray();
			tray.remove(trayIcon);
			Platform.exit();
		});

		javax.swing.SwingUtilities.invokeLater(StageInitializer::addAppToTray);

		FXMLLoader loader = new FXMLLoader(CLASS_LOADER.getResource(FXML_MAIN));
		BorderPane decorateFram = loader.load();
		decorateFram.getStylesheets().add(stylesheet);
		Scene scene = new Scene(decorateFram);
		scene.setOnMousePressed(event -> {
			xOffset = stage.getX() - event.getScreenX();
			yOffset = stage.getY() - event.getScreenY();
		});
		scene.setOnMouseDragged(event -> {
			stage.setX(event.getScreenX() + xOffset);
			stage.setY(event.getScreenY() + yOffset);
		});
		scene.getStylesheets().setAll(decorateFram.getStylesheets());
		scene.setFill(Color.TRANSPARENT);

		loader = new FXMLLoader(CLASS_LOADER.getResource(FXML_PART_HEADER));
		BorderPane header = loader.load();
		header.getStylesheets().add(stylesheet);
		ControlerHeader controlerHeader = loader.getController();
		controlerHeader.setStage(stage);

		loader = new FXMLLoader(CLASS_LOADER.getResource(FXML_PART_CURRENT_TASKS));
		BorderPane currentTasks = loader.load();
		currentTasks.getStylesheets().add(stylesheet);

		loader = new FXMLLoader(CLASS_LOADER.getResource(FXML_PART_SUB_PART));
		TabPane subPart = loader.load();
		subPart.getStylesheets().add(stylesheet);

		stage.heightProperty().addListener(StageInitializer::clipFram);

		initBackendTask.setOnSucceeded(e -> {
			VBox newRoot = new VBox(header, currentTasks, subPart);
			newRoot.setId("rootMain");
			decorateFram.setCenter(newRoot);
			clipFram(null, null, null);
			LOG.debug("Init backend end");
		});
		new Thread(initBackendTask).start();

		stage.setScene(scene);
		stage.show();
		clipFram(null, null, null);

		// String title = "Congratulations sir";
		// String message = "You've successfully created your first Tray Notification";
		// Notification notification = Notifications.SUCCESS;
		//
		// TrayNotification tray = new TrayNotification();
		// tray.setTitle(title);
		// tray.setMessage(message);
		// tray.setNotification(notification);
		// tray.showAndWait();
	}

	public static String getStylesheet() {
		return stylesheet;
	}

	public static <T> void clipFram(ObservableValue<? extends T> observable, T oldValue, T newValue) {
		try {
			BorderPane bp = (BorderPane) stage.getScene().getRoot();
			int margin = 2;
			double borderRadius = 0;
			try {
				margin += bp.getPadding().getBottom() * 2;
			}
			catch (NullPointerException e) {
				// No padding
			}
			try {
				margin += bp.getBorder().getStrokes().get(0).getWidths().getLeft();
				borderRadius = bp.getBorder().getStrokes().get(0).getRadii().getBottomLeftHorizontalRadius() / 2;
			}
			catch (NullPointerException e) {
				// No border
			}

			// LOG.debug("margin [{}] borderRadius[{}]", margin, borderRadius);

			javafx.scene.shape.Rectangle rect = new javafx.scene.shape.Rectangle(0, 0, stage.getWidth() - margin, stage.getHeight() - margin);
			rect.setArcHeight(borderRadius);
			rect.setArcWidth(borderRadius);
			stage.getScene().lookup("#rootMain").setClip(rect);
		}
		catch (Exception e) {
			LOG.debug("clip error", e);
		}

	}

	// public static void showSystrayMessage(String message) {
	// if (trayIcon != null) {
	// javax.swing.SwingUtilities.invokeLater(() -> trayIcon.displayMessage(Constant.APP_NAME, message, java.awt.TrayIcon.MessageType.NONE));
	// }
	// }

	// public static void critalError(String message, Exception e) {
	// log.error(message, e);
	// StageInitializer2.errorMessagePopup(e.getMessage());
	// System.exit(-1);
	// }

	// public static void errorMessagePopup(String text) {
	// alert = new Alert(AlertType.ERROR);
	// alert.setHeaderText(null);
	// alert.setTitle(Constant.APP_NAME);
	// alert.setContentText(text);
	//
	// Stage stage = (Stage) alert.getDialogPane().getScene().getWindow();
	// stage.getIcons().add(new Image(classLoader.getResource("com/alphadev/dictator/client/ui/img/main.png").toString()));
	// alert.showAndWait();
	// }

	public static Stage getStage() {
		return stage;
	}

	private static void showStage() {
		if (stage != null) {
			stage.show();
			stage.toFront();
		}
	}

	private static Rectangle getScreenBounds() {
		Insets si = Toolkit.getDefaultToolkit().getScreenInsets(GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice().getDefaultConfiguration());
		Rectangle sb = GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice().getDefaultConfiguration().getBounds();

		sb.x += si.left;
		sb.y += si.top;
		sb.width -= si.left + si.right;
		sb.height -= si.top + si.bottom;
		return sb;
	}

	private static void addAppToTray() {
		try {
			java.awt.Toolkit.getDefaultToolkit();

			if (!java.awt.SystemTray.isSupported()) {
				LOG.error("No system tray support, application exiting.");
				Platform.exit();
			}

			java.awt.SystemTray tray = java.awt.SystemTray.getSystemTray();
			java.awt.Image image = ImageIO.read(CLASS_LOADER.getResourceAsStream(SYS_TRAY_ICON));
			java.awt.TrayIcon trayIcon = new java.awt.TrayIcon(image);
			trayIcon.addActionListener(event -> Platform.runLater(StageInitializer::showStage));

			java.awt.MenuItem openItem = new java.awt.MenuItem(Constant.APP_NAME);
			openItem.addActionListener(event -> Platform.runLater(StageInitializer::showStage));

			java.awt.MenuItem exitItem = new java.awt.MenuItem("Quit");
			exitItem.addActionListener(event -> {
				stage.getOnCloseRequest().handle(null);
			});

			final java.awt.PopupMenu popup = new java.awt.PopupMenu();
			popup.add(openItem);
			popup.addSeparator();
			popup.add(exitItem);
			trayIcon.setPopupMenu(popup);
			StageInitializer.trayIcon = trayIcon;

			tray.add(trayIcon);
		}
		catch (java.awt.AWTException | IOException e) {
			LOG.error("Unable to init system tray.", e);
		}
	}

	private static void hackTooltipStartTiming(double millis) {
		try {
			Field fieldBehavior = Tooltip.class.getDeclaredField("BEHAVIOR");
			fieldBehavior.setAccessible(true);
			Object objBehavior = fieldBehavior.get(new Tooltip());

			Field fieldTimer = objBehavior.getClass().getDeclaredField("activationTimer");
			fieldTimer.setAccessible(true);
			Timeline objTimer = (Timeline) fieldTimer.get(objBehavior);

			objTimer.getKeyFrames().clear();
			objTimer.getKeyFrames().add(new KeyFrame(new Duration(millis)));
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}
}
