package com.alphadev.dictator;

public class Constant {

	public static final String	APP_NAME		= "Dictator";
	public static final String	NO_PROJET_COLOR	= "#7F7F7F";
	public static final String	NO_PROJET_DESC	= "No Projet";
	public static final String	NO_PROJET_KEY	= "-";
}
