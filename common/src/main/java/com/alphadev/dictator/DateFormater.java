package com.alphadev.dictator;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateFormater {
	private static final DateFormat DEFAULT_FORMATER = new SimpleDateFormat("HH:mm dd/MM/yyyy");

	public static String format(String format, Date date) {
		DateFormat formater = new SimpleDateFormat(format);
		return formater.format(date);
	}

	public static String format(Date date) {
		return DEFAULT_FORMATER.format(date);
	}
}
