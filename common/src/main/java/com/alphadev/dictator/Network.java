package com.alphadev.dictator;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Base64;
import java.util.Map;
import java.util.Scanner;
import java.util.StringJoiner;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.alphadev.dictator.exception.DictatorRuntimeException;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

public class Network {
	public static final String	USER_AGENT	= "com.alphadev.dictator";
	public static final String	CHARSET	= java.nio.charset.StandardCharsets.UTF_8.name();
	private static final Logger	LOG_HTTP	= LogManager.getLogger("logHttp");

	public static void sleep(long millis) {
		try {
			Thread.sleep(millis);
		}
		catch (InterruptedException e) {
			throw new DictatorRuntimeException(e);
		}
	}

	public static void quietCloseStream(InputStream inputStream) {
		if (inputStream != null) {
			try {
				inputStream.close();
			}
			catch (IOException e) {
			}
		}
	}

	public static URL makeURL(String url) {
		try {
			return new URL(url);
		}
		catch (MalformedURLException e) {
			throw new RuntimeException(e);
		}
	}

	public static <T> T getAndReadResponse(URL url, Map<String, String> arguments, String login, String pass, Class<T> type) {
		HttpURLConnection conn = null;
		OutputStream outputStream = null;
		InputStream inputStream = null;
		Scanner scanner = null;
		T response = null;

		try {
			StringJoiner rawData = new StringJoiner("&");
			for (Map.Entry<String, String> entry : arguments.entrySet()) {
				rawData.add(URLEncoder.encode(entry.getKey(), CHARSET) + "=" + URLEncoder.encode(entry.getValue(), CHARSET));
			}
			String encodedData = rawData.toString();

			url = new URL(url.toString() + "?" + encodedData);
			String basicAuth = Base64.getEncoder().encodeToString((login + ":" + pass).getBytes());

			conn = (HttpURLConnection) url.openConnection();
			conn.setDoOutput(true);
			conn.setRequestMethod("GET");
			conn.setRequestProperty("Authorization", "Basic " + basicAuth);

			inputStream = conn.getInputStream();
			scanner = new Scanner(inputStream);
			scanner.useDelimiter("\\A");
			String result = scanner.hasNext() ? scanner.next() : "";

			LOG_HTTP.trace("[{}] response [{}]-[{}]", url, type.getSimpleName(), result);

			ObjectMapper mapper = getObjectMapper();
			response = mapper.readValue(result, type);
		}
		catch (IOException e) {
			if (conn != null) {
				LOG_HTTP.error("[{}] error [{}]", url, e.getMessage());
				inputStream = conn.getErrorStream();
				if (inputStream != null) {
					scanner = new Scanner(inputStream);
					scanner.useDelimiter("\\A");
					String result = scanner.hasNext() ? scanner.next() : "";
					LOG_HTTP.error("[{}] error server message [{}]", url, result);
				}
			}

			throw new DictatorRuntimeException(e);
		}
		finally {
			try {
				if (outputStream != null) {
					outputStream.close();
				}
				if (inputStream != null) {
					inputStream.close();
				}
				if (scanner != null) {
					scanner.close();
				}
			}
			catch (IOException e) {
			}
		}
		return response;
	}

	public static ObjectMapper getObjectMapper() {
		ObjectMapper mapper = new ObjectMapper();
		mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		mapper.setSerializationInclusion(Include.NON_NULL);
		mapper.setSerializationInclusion(Include.NON_DEFAULT);
		return mapper;
	}

	public static <T> T postAndReadResponse(URL url, String postData, String login, String pass, Class<T> type) {
		return requestWithDataAndReadResponse(url, postData, login, pass, type, "POST");
	}

	public static <T> T putAndReadResponse(URL url, String postData, String login, String pass, Class<T> type) {
		return requestWithDataAndReadResponse(url, postData, login, pass, type, "PUT");
	}

	public static <T> T requestWithDataAndReadResponse(URL url, String postData, String login, String pass, Class<T> type, String requestMethod) {
		HttpURLConnection conn = null;
		OutputStream outputStream = null;
		InputStream inputStream = null;
		Scanner scanner = null;
		T response = null;

		try {
			String basicAuth = Base64.getEncoder().encodeToString((login + ":" + pass).getBytes());

			conn = (HttpURLConnection) url.openConnection();
			conn.setDoOutput(true);
			conn.setRequestMethod(requestMethod);

			conn.setRequestProperty("Content-Type", "Content-Type: application/json");
			conn.setRequestProperty("Content-Length", String.valueOf(postData.length()));
			conn.setRequestProperty("Authorization", "Basic " + basicAuth);
			outputStream = conn.getOutputStream();
			outputStream.write(postData.getBytes());

			inputStream = conn.getInputStream();
			scanner = new Scanner(inputStream);
			scanner.useDelimiter("\\A");
			String result = scanner.hasNext() ? scanner.next() : "";

			LOG_HTTP.trace("[{}] response [{}]", url, result);

			ObjectMapper mapper = getObjectMapper();
			response = mapper.readValue(result, type);
		}
		catch (IOException e) {
			if (conn != null) {
				LOG_HTTP.error("[{}] error [{}]", url, e.getMessage());
				inputStream = conn.getErrorStream();
				if (inputStream != null) {
					scanner = new Scanner(inputStream);
					scanner.useDelimiter("\\A");
					String result = scanner.hasNext() ? scanner.next() : "";
					LOG_HTTP.error("[{}] error server message [{}]", url, result);
				}
			}

			throw new DictatorRuntimeException(e);
		}
		finally {
			try {
				if (outputStream != null) {
					outputStream.close();
				}
				if (inputStream != null) {
					inputStream.close();
				}
				if (scanner != null) {
					scanner.close();
				}
			}
			catch (IOException e) {
			}
		}
		return response;
	}
}
