package com.alphadev.dictator.exception;

public class DictatorRuntimeException extends RuntimeException {

	private static final long serialVersionUID = -5446004370048763287L;

	public DictatorRuntimeException() {
		super();
	}

	public DictatorRuntimeException(String message) {
		super(message);
	}

	public DictatorRuntimeException(String message, Throwable throwable) {
		super(message, throwable);
	}

	public DictatorRuntimeException(Throwable throwable) {
		super(throwable);
	}

}
