package com.alphadev.dictator.exception;

public class DictatorException extends Exception {

	private static final long serialVersionUID = 8117105541158008464L;

	public DictatorException() {
		super();
	}

	public DictatorException(String message) {
		super(message);
	}

	public DictatorException(String message, Throwable throwable) {
		super(message, throwable);
	}

}
