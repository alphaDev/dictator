package com.alphadev.dictator.exception;

public class DictatorUserPropertiesException extends DictatorRuntimeException {

	private static final long serialVersionUID = -6235317839728468812L;

	public DictatorUserPropertiesException() {
		super();
	}

	public DictatorUserPropertiesException(String message, Throwable throwable) {
		super(message, throwable);
	}

	public DictatorUserPropertiesException(String message) {
		super(message);
	}

}
