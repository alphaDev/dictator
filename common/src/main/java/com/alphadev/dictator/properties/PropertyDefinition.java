package com.alphadev.dictator.properties;

import java.time.Duration;
import java.util.Arrays;

import org.apache.commons.lang3.StringUtils;

public class PropertyDefinition {
	private String	name;
	private boolean	mandatory;
	private Type	type;
	private String	rawValue;

	public PropertyDefinition() {
	}

	@Override
	public String toString() {
		return String.format("/PD [%s] [%s] [%s] [%s]/", name, mandatory, type, rawValue);
	}

	public PropertyDefinition(String name, boolean mandatory, Type type) {
		this.name = name;
		this.mandatory = mandatory;
		this.type = type;
	}

	public String getName() {
		return name;
	}

	public void setRawValue(String rawValue) {
		this.rawValue = rawValue;
	}

	public boolean missingValue() {
		return mandatory && StringUtils.isBlank(rawValue);
	}

	public <T> T getValue() {
		T value = null;
		if (StringUtils.isNotBlank(rawValue)) {
			switch (type) {
				case LIST:
					value = (T) Arrays.asList(rawValue.split("[,;]"));
				break;
				case BOOL:
					value = (T) Boolean.valueOf("true".equals(rawValue));
				break;
				case DURATION:
					String tmp = ("PT" + rawValue).toUpperCase();
					value = (T) Long.valueOf(Duration.parse(tmp).toMillis());
				break;
				case STRING:
				default:
					value = (T) rawValue;
				break;
			}
		}

		return value;
	}

	public enum Type {
		LIST, BOOL, DURATION, STRING

	}
}
