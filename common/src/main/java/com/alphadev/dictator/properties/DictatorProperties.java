package com.alphadev.dictator.properties;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.alphadev.dictator.Network;
import com.alphadev.dictator.exception.DictatorRuntimeException;
import com.alphadev.dictator.exception.DictatorUserPropertiesException;

public class DictatorProperties {
	private static final Logger				LOG							= LogManager.getLogger(DictatorProperties.class);
	private static final String				USER_PROPERTIES				= "user.properties";
	private static final String				DEFAULT_PROPERTIES			= "default.properties";
	private static final PropertyDefinition	DEFAULT_PROPERTY_DEFINITION	= new PropertyDefinition();

	private Map<String, PropertyDefinition>	propertyDefinitions			= new HashMap<>();
	private static DictatorProperties		instance;

	private DictatorProperties() {
	}

	public static void load() throws DictatorUserPropertiesException {
		addPropertyDefinitions(new PropertyDefinition(CommonProperties.WORKING_HOURS, true, PropertyDefinition.Type.DURATION));
		getInstance().loadPropertiesFile();
	}

	public static void reload() throws DictatorUserPropertiesException {
		getInstance().loadPropertiesFile();
	}

	public static void addPropertyDefinitions(PropertyDefinition... propertyDefinitions) {
		for (PropertyDefinition propertyDefinition : propertyDefinitions) {
			getInstance().propertyDefinitions.put(propertyDefinition.getName(), propertyDefinition);
		}
	}

	public static <T> T get(String property) {
		PropertyDefinition propertyDefinition = getInstance().propertyDefinitions.getOrDefault(property, DEFAULT_PROPERTY_DEFINITION);
		return propertyDefinition.getValue();

	}

	private static DictatorProperties getInstance() {
		if (instance == null) {
			instance = new DictatorProperties();
		}
		return instance;
	}

	private void loadPropertiesFile() throws DictatorUserPropertiesException {
		InputStream inputStream = null;
		Properties defaultProp = new Properties();

		try {
			inputStream = getClass().getClassLoader().getResourceAsStream(DEFAULT_PROPERTIES);
			if (inputStream != null) {
				defaultProp.load(inputStream);
			}
		}
		catch (IOException e) {
			LOG.error("Load [{}] file fail [{}].", DEFAULT_PROPERTIES, e.getMessage(), e);
			throw new DictatorRuntimeException("Fatal error : Cannot load default properties.", e);
		}
		finally {
			Network.quietCloseStream(inputStream);
		}

		Properties prop = new Properties(defaultProp);
		try {
			inputStream = new FileInputStream(USER_PROPERTIES);
			prop.load(inputStream);
		}
		catch (IOException e) {
			LOG.error("Load [{}] file fail [{}].", USER_PROPERTIES, e.getMessage(), e);
			throw new DictatorUserPropertiesException("Cannot load user properties file.", e);
		}
		finally {
			Network.quietCloseStream(inputStream);
		}

		for (PropertyDefinition propertyDefinition : propertyDefinitions.values()) {
			propertyDefinition.setRawValue(prop.getProperty(propertyDefinition.getName()));
		}

		List<String> missingProperties = new ArrayList<>();

		List<PropertyDefinition> toCheckSorted = new ArrayList<>(propertyDefinitions.values());
		Collections.sort(toCheckSorted, (pd1, pd2) -> {
			return pd1.getName().compareTo(pd2.getName());
		});

		for (PropertyDefinition propertyDefinition : toCheckSorted) {
			if (propertyDefinition.missingValue()) {
				missingProperties.add(propertyDefinition.getName());
			}
		}

		if (!missingProperties.isEmpty()) {
			String missingPropertiesString = String.join("\n * ", missingProperties);
			throw new DictatorUserPropertiesException(String.format("Missing properties in %s :\n * %s.", USER_PROPERTIES, missingPropertiesString));
		}
	}

}
