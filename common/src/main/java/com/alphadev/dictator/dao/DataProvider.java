package com.alphadev.dictator.dao;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.alphadev.dictator.dao.item.Day;
import com.alphadev.dictator.dao.item.Session;
import com.alphadev.dictator.dao.item.Task;
import com.alphadev.dictator.exception.DictatorRuntimeException;
import com.fasterxml.jackson.databind.ObjectMapper;

public final class DataProvider {

	private static final Logger					LOG				= LogManager.getLogger(DataProvider.class);
	private final static ObjectMapper			MAPPER			= new ObjectMapper();
	private final static File					RETENTION_FILE	= new File("rentention.json");

	private static Map<String, List<Session>>	sessionByTask	= new HashMap<>();
	private static DataVault					dataVault		= new DataVault();
	private static List<DataListener>			dataListeners	= new ArrayList<>();
	private static boolean						hasRestoreData;
	private static boolean						dataChange;

	private static void save() {
		LOG.trace("save DataProvider retention");
		try {
			MAPPER.writeValue(RETENTION_FILE, dataVault);
		}
		catch (IOException e) {
			LOG.warn("Cannot save rentention file[{}]", RETENTION_FILE.getAbsolutePath(), e);
		}
	}

	public synchronized static void init() {
		new Timer(true).scheduleAtFixedRate(new DataChangeEventHandler(), 3000l, 500l);
		LOG.info("load DataProvider from retention");
		try {
			dataVault = MAPPER.readValue(RETENTION_FILE, DataVault.class);
			dataVault.getSessions().values().forEach(DataProvider::updateSessionByTaskIndex);
			hasRestoreData = true;
			dataChange = true;
		}
		catch (FileNotFoundException e) {
			LOG.info("Rentention file [{}] not found. Skip load retention.", RETENTION_FILE.getAbsolutePath());
		}
		catch (IOException e) {
			LOG.warn("Cannot restore rentention file[{}]", RETENTION_FILE.getAbsolutePath(), e);
		}
	}

	private static void updateSessionByTaskIndex(Session session) {
		if (StringUtils.isNotEmpty(session.getTaskKey())) {
			List<Session> sessionByTaskEntry = sessionByTask.getOrDefault(session.getTaskKey(), new ArrayList<>());
			sessionByTaskEntry.add(session);
			sessionByTask.put(session.getTaskKey(), sessionByTaskEntry);
		}
	}

	public static boolean isHasRestoreData() {
		return hasRestoreData;
	}

	public synchronized static void addDataListener(DataListener dataListener) {
		dataListeners.add(dataListener);
	}

	public synchronized static List<Session> getSessions() {
		return new ArrayList<Session>(dataVault.getSessions().values());
	}

	public synchronized static Session getSession(String id) {
		return dataVault.getSessions().getOrDefault(id, null);
	}

	public static List<Session> getSessionsByTask(String key) {
		return sessionByTask.getOrDefault(key, new ArrayList<>());
	}

	protected synchronized static void syncSessions(List<Session> sessions) {
		dataVault.getSessions().clear();
		sessions.stream().forEach(DataProvider::createSession);
	}

	protected synchronized static void createSession(Session session) {
		if (StringUtils.isBlank(session.getId())) {
			throw new DictatorRuntimeException(String.format("Trying to save a session without id."));
		}

		Session oldSession = dataVault.getSessions().get(session.getId());
		if (oldSession != null && sessionByTask.containsKey(oldSession.getTaskKey())) {
			sessionByTask.get(oldSession.getTaskKey()).remove(oldSession);
		}
		updateSessionByTaskIndex(session);

		dataVault.getSessions().put(session.getId(), session);
		dataChange = true;
	}

	protected synchronized static void deleteSession(Session session) {
		if (StringUtils.isBlank(session.getId())) {
			throw new DictatorRuntimeException(String.format("Trying to delete a session without id."));
		}

		Session oldSession = dataVault.getSessions().get(session.getId());
		if (oldSession != null && sessionByTask.containsKey(oldSession.getTaskKey())) {
			sessionByTask.get(oldSession.getTaskKey()).remove(oldSession);
		}

		dataVault.getSessions().remove(session.getId());
		dataChange = true;
		LOG.trace("delete session : [{}]", session);
	}

	public synchronized static List<Task> getTasks() {
		return new ArrayList<Task>(dataVault.getTasks().values());
	}

	public synchronized static Task getTask(String id) {
		return dataVault.getTasks().getOrDefault(id, null);
	}

	protected synchronized static void syncTasks(List<Task> tasks) {
		dataVault.getTasks().clear();
		tasks.stream().forEach(DataProvider::createTask);
	}

	protected synchronized static void createTask(Task task) {
		if (StringUtils.isBlank(task.getKey())) {
			throw new DictatorRuntimeException(String.format("Trying to save a task without id."));
		}
		dataVault.getTasks().put(task.getKey(), task);
		dataChange = true;
	}

	protected synchronized static void updateTasks(List<Task> tasks) {
		for (Task task : tasks) {
			updateTask(task);
		}
	}

	protected synchronized static void updateTask(Task task) {
		if (StringUtils.isBlank(task.getKey())) {
			throw new DictatorRuntimeException(String.format("Trying to update a task without id."));
		}
		if (!dataVault.getTasks().containsKey(task.getKey())) {
			throw new DictatorRuntimeException(String.format("Unknown task with id [%s].", task.getKey()));
		}

		dataVault.getTasks().put(task.getKey(), task);
		dataChange = true;
	}

	public synchronized static Session getCurrentSession() {
		return dataVault.getSessions().values().stream().filter(s -> !s.close()).findFirst().orElse(null);
	}

	public synchronized static List<Task> getWorkingTasks() {
		return dataVault.getTasks().values().stream().filter(Task::running).collect(Collectors.toList());
	}

	public synchronized static List<Session> getTodayCloseSession() {
		Calendar startDate = Calendar.getInstance();
		startDate.set(Calendar.HOUR_OF_DAY, 0);
		startDate.set(Calendar.MINUTE, 0);
		startDate.set(Calendar.SECOND, 0);
		startDate.set(Calendar.MILLISECOND, 0);
		return dataVault.getSessions().values().stream().filter(s -> s.getStart().after(startDate.getTime())).collect(Collectors.toList());
	}

	public synchronized static Map<Day, List<Session>> getCurrentWeekSummary() {
		return getWeekSummary(Calendar.getInstance().getTime());
	}

	public synchronized static Map<Day, List<Session>> getWeekSummary(Date weekToAsk) {
		Map<Day, List<Session>> summary = new HashMap<>();

		for (Day day : Day.values()) {
			summary.put(day, new ArrayList<>());
		}

		Calendar startDate = Calendar.getInstance();
		startDate.setTime(weekToAsk);
		startDate.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
		startDate.set(Calendar.HOUR_OF_DAY, 0);
		startDate.set(Calendar.MINUTE, 0);
		startDate.set(Calendar.SECOND, 0);
		startDate.set(Calendar.MILLISECOND, 0);
		Calendar endDate = (Calendar) startDate.clone();
		endDate.add(Calendar.DATE, 7);

		// LOG.debug("getWeekSummary startDate [{}]", String.format("%2$td/%2$tm/%2$ty-%2$tT)", "", startDate.getTime()));
		// LOG.debug("getWeekSummary endDate [{}]", String.format("%2$td/%2$tm/%2$ty-%2$tT)", "", endDate.getTime()));

		for (Session session : getSessions()) {
			Calendar sessionStart = Calendar.getInstance();
			sessionStart.setTime(session.getStart());

			if (session.close() && sessionStart.after(startDate) && sessionStart.before(endDate)) {
				Day startDay = Day.fromCalendarDay(sessionStart.get(Calendar.DAY_OF_WEEK));
				summary.get(startDay).add(session);
			}

		}

		return summary;
	}

	private static class DataVault implements Serializable {

		private static final long		serialVersionUID	= -7308075556058985710L;

		private Map<String, Task>		tasks				= new HashMap<>();
		private Map<String, Session>	sessions			= new HashMap<>();

		public Map<String, Task> getTasks() {
			return tasks;
		}

		public Map<String, Session> getSessions() {
			return sessions;
		}

	}

	private static class DataChangeEventHandler extends TimerTask {

		@Override
		public void run() {
			if (dataChange) {
				Session currentSession = null;
				List<Session> todayCloseSession = null;
				Map<Day, List<Session>> weekSummary = null;
				List<Task> workingTasks = null;
				Map<String, Task> copyTasks;
				Map<String, Session> copySession;
				synchronized (DataProvider.class) {
					currentSession = getCurrentSession();
					todayCloseSession = getTodayCloseSession();
					weekSummary = getCurrentWeekSummary();
					workingTasks = getWorkingTasks();
					copyTasks = dataVault.getTasks();
					copySession = dataVault.getSessions();
					DataProvider.save();
					dataChange = false;
				}

				for (DataListener dataListener : DataProvider.dataListeners) {
					dataListener.onSetCurrentSession(currentSession);
				}
				for (DataListener dataListener : DataProvider.dataListeners) {
					dataListener.onSetTodayCloseSession(todayCloseSession);
				}
				for (DataListener dataListener : DataProvider.dataListeners) {
					dataListener.onSetWeekSummary(weekSummary);
				}
				for (DataListener dataListener : DataProvider.dataListeners) {
					dataListener.onSetWorkingTasks(workingTasks);
				}
				for (DataListener dataListener : DataProvider.dataListeners) {
					dataListener.onChangeTasks(copyTasks);
				}
				for (DataListener dataListener : DataProvider.dataListeners) {
					dataListener.onChangeSession(copySession);
				}

			}
		}
	}

}
