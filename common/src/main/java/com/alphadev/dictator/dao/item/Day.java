package com.alphadev.dictator.dao.item;

import java.util.Calendar;

import org.apache.commons.lang3.text.WordUtils;

import com.alphadev.dictator.exception.DictatorRuntimeException;

public enum Day {
	MONDAY(0), TUESDAY(1), WEDNESDAY(2), THURSDAY(3), FRIDAY(4), SATURDAY(5), SUNDAY(6);

	private int		index;
	private String	catName;

	private Day(int index) {
		this.index = index;
		this.catName = WordUtils.capitalize(this.name().substring(0, 3));
	}

	public int getIndex() {
		return index;
	}

	public String getCatName() {
		return catName;
	}

	public static Day fromCalendarDay(int calendarDay) {
		switch (calendarDay) {
			case Calendar.MONDAY:
				return MONDAY;
			case Calendar.TUESDAY:
				return TUESDAY;
			case Calendar.WEDNESDAY:
				return WEDNESDAY;
			case Calendar.THURSDAY:
				return THURSDAY;
			case Calendar.FRIDAY:
				return FRIDAY;
			case Calendar.SATURDAY:
				return SATURDAY;
			case Calendar.SUNDAY:
				return SUNDAY;

			default:
				throw new DictatorRuntimeException("Cannot cast [" + calendarDay + "] to Day");
		}
	}
}
