package com.alphadev.dictator.dao.item.property;

import java.io.Serializable;

import com.alphadev.dictator.plugin.PluginInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;

import javafx.scene.control.Control;
import javafx.scene.control.TextField;

public class PropertyInfo implements Serializable {
	private static final long	serialVersionUID	= -5931912320823561550L;

	private String				name;
	private Type				type;
	private PluginInfo			from;
	private TextField			control;

	public PropertyInfo(String name, Type type, PluginInfo from) {
		this.name = name;
		this.type = type;
		this.from = from;
	}

	public String getName() {
		return name;
	}

	public Type getType() {
		return type;
	}

	public PluginInfo getFrom() {
		return from;
	}

	@JsonIgnore
	public Control getControl() {
		if (control == null) {
			switch (type) {
				case STRING:
					control = new TextField();
				break;
			}
		}
		return control;
	}

	public void setControlValue(Object value) {
		switch (type) {
			case STRING:
				TextField c = (TextField) getControl();
				if (value != null) {
					c.setText(value.toString());
				}
				else {
					c.setText("");
				}
			break;
		}
	}

	public void resetControl() {
		switch (type) {
			case STRING:
				TextField c = (TextField) getControl();
				c.setText("");
			break;
		}
	}

	public enum Type {
		STRING(String.class);

		Class<?> typeClass;

		Type(Class<?> typeClass) {
			this.typeClass = typeClass;
		}

		public Class<?> getTypeClass() {
			return typeClass;
		}
	}

}
