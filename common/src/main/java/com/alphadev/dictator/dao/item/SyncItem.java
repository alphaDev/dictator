package com.alphadev.dictator.dao.item;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import com.alphadev.dictator.plugin.PluginInfo;

public abstract class SyncItem implements Serializable {
	private static final long	serialVersionUID	= 2055033960019269327L;

	private Date				lastChange;
	protected boolean			toSync;
	protected boolean			wasSync;
	private Set<PluginInfo>		fromPlugin			= new HashSet<>();

	public Date getLastChange() {
		return lastChange;
	}

	public void setLastChange(Date lastChange) {
		this.lastChange = lastChange;
	}

	public Set<PluginInfo> getFromPlugin() {
		return fromPlugin;
	}

	public void syncDone(PluginInfo fromPlugin) {
		this.fromPlugin.add(fromPlugin);
		this.wasSync = true;
	}

	public void resetSync() {
		this.wasSync = false;
		this.toSync = false;
	}

	public void askSync() {
		this.wasSync = false;
		this.toSync = true;
	}

	public boolean mustSync() {
		return toSync;
	}

	public boolean wasNotSync() {
		return this.toSync && !this.wasSync;
	}

	public boolean lastChangeBetween(Date since, Date to) {
		return getLastChange().after(since) && (to == null || getLastChange().before(to));
	}

}
