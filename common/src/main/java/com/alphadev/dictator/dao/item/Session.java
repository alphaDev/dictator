package com.alphadev.dictator.dao.item;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.alphadev.dictator.DurationFormater;
import com.alphadev.dictator.dao.DataProvider;
import com.alphadev.dictator.dao.item.property.PropertyInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;

public class Session extends SyncItem implements Serializable {
	private static final long						serialVersionUID	= 4993217263097211273L;
	private static final Logger						LOG					= LogManager.getLogger(Session.class);
	private static final Map<String, PropertyInfo>	CUSTOM_PROPERTIES	= new HashMap<>();

	private final Map<String, Object>				customValue			= new HashMap<>();
	private String									id;
	private String									description;
	private Date									start;
	private Date									stop;
	private String									taskKey;

	public Session() {
	}

	@Override
	public String toString() {
		return String.format("/S (%1$10s-%4$10s) (%2$td/%2$tm/%2$ty-%2$tT) (%3$9s) (%8$td/%8$tm/%8$ty-%8$tT : %6$s-%7$s) (%5$s) /", id, start, DurationFormater.format(calcDuration()), getTaskKey(), getDescription(), toSync, wasSync, getLastChange());
	}

	@Override
	public boolean equals(Object obj) {
		if (obj != null && obj instanceof Session) {
			return StringUtils.equals(id, ((Session) obj).id);
		}
		return false;
	}

	public static boolean same(Session session01, Session session02) {
		if (session01 == session02) {
			return true;
		}
		if (session01 == null || session02 == null) {
			return false;
		}
		return session01.id.equals(session02.id) && session01.start.equals(session02.start) && ((session01.stop == null && session02.stop == null) || (session01.stop != null && session01.stop.equals(session02.stop))) && (StringUtils.equals(session01.getTaskKey(), session02.getTaskKey()));
	}

	public static int compare(Session s1, Session s2) {
		return s1.getStart().compareTo(s2.getStart()) * -1;
	}

	@JsonIgnore
	public static void setCustomProp(List<PropertyInfo> properties) {
		for (PropertyInfo propertyInfo : properties) {
			CUSTOM_PROPERTIES.put(propertyInfo.getName(), propertyInfo);
		}
	}

	@JsonIgnore
	public static Collection<PropertyInfo> getCustomProp() {
		return CUSTOM_PROPERTIES.values();
	}

	@JsonIgnore
	public <T> T getCustomValue(String prop, Class<T> type) {
		if (!CUSTOM_PROPERTIES.containsKey(prop)) {
			LOG.warn("unknown custom value : [{}]", prop);
			return null;
		}
		if (!CUSTOM_PROPERTIES.get(prop).getType().getTypeClass().equals(type)) {
			LOG.warn("Custom value [{}] ask is not of type [{}] is type [{}]", prop, type, CUSTOM_PROPERTIES.get(prop).getType().getTypeClass());
			return null;
		}
		return (T) customValue.getOrDefault(prop, null);
	}

	@JsonIgnore
	public <T> void setCustomValue(String prop, T value, Class<T> type) {
		if (!CUSTOM_PROPERTIES.containsKey(prop)) {
			LOG.warn("unknown custom value : [{}]", prop);
			return;
		}
		if (!CUSTOM_PROPERTIES.get(prop).getType().getTypeClass().equals(type)) {
			LOG.warn("Custom value [{}] ask is not of type [{}] is type [{}]", prop, type, CUSTOM_PROPERTIES.get(prop).getType().getTypeClass());
			return;
		}

		// LOG.debug("[{}] put custom value[{}] : [{}]", this, prop, value);
		customValue.put(prop, value);
	}

	public Map<String, Object> getCustomValue() {
		return customValue;
	}

	public Date getStart() {
		return start;
	}

	public void setStart(Date start) {
		this.start = start;
	}

	public long calcDuration() {
		if (stop == null) {
			long currentTimeMillis = System.currentTimeMillis();
			long duration = currentTimeMillis - start.getTime();
			duration = (duration < 0) ? 0 : duration;
			return duration;
		}
		else {
			long duration = stop.getTime() - start.getTime();
			return duration;
		}
	}

	public boolean close() {
		return stop != null;
	}

	public boolean running() {
		return stop == null;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Date getStop() {
		return stop;
	}

	public void setStop(Date stop) {
		this.stop = stop;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getTaskKey() {
		return taskKey;
	}

	public void setTaskKey(String taskKey) {
		this.taskKey = taskKey;
	}

	@JsonIgnore
	public Task task() {
		if (StringUtils.isEmpty(taskKey)) {
			return null;
		}
		return DataProvider.getTask(taskKey);
	}

}
