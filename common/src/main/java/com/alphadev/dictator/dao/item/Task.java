package com.alphadev.dictator.dao.item;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.alphadev.dictator.DateFormater;
import com.alphadev.dictator.DurationFormater;
import com.alphadev.dictator.dao.DataProvider;
import com.fasterxml.jackson.annotation.JsonIgnore;

public class Task extends SyncItem implements Serializable {
	static final Logger			LOG					= LogManager.getLogger(Task.class);
	private static final long	serialVersionUID	= 7439893845018208198L;

	private String				description;
	private String				key;
	private String				version;
	private String				color;

	@Override
	public boolean equals(Object obj) {
		if (obj == null || !Task.class.isAssignableFrom(obj.getClass())) {
			return false;
		}
		Task t = (Task) obj;
		return StringUtils.equals(key, t.key);
	}

	@Override
	public String toString() {
		return String.format("/T (%s) (%s-%s) (%s) (%s) (%s)/", key, version, color, DurationFormater.format(calcTimeSpend()), description, DateFormater.format(getLastChange()));
	}

	public static boolean same(Task oldTasks, Task currentTask) {
		if (oldTasks == currentTask) {
			return true;
		}
		if (oldTasks == null || currentTask == null) {
			return false;
		}
		return oldTasks.equals(currentTask);
	}

	public static int compareByLastSession(Task s1, Task s2) {
		return s1.lastSessionStart().compareTo(s2.lastSessionStart()) * -1;
	}

	public Session openSession() {
		return sessions().stream().filter(Session::running).findAny().orElse(null);
	}

	public Long calcTimeSpend() {
		return sessions().stream().filter(Session::close).mapToLong(Session::calcDuration).sum();
	}

	public Date lastSessionStart() {
		if (sessions().isEmpty()) {
			Date tmp = Calendar.getInstance().getTime();
			tmp.setTime(0);
			return tmp;
		}

		final Date start = sessions().stream().min(Session::compare).get().getStart();
		return start;

	}

	public boolean running() {
		return sessions().stream().anyMatch(Session::running);

	}

	public String getDescription() {
		return description;
	}

	public String getKey() {
		return key;
	}

	public String getVersion() {
		return version;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	@JsonIgnore
	public List<Session> sessions() {
		return DataProvider.getSessionsByTask(key);
	}

}
