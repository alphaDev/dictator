package com.alphadev.dictator.dao;

import java.util.List;
import java.util.Map;

import com.alphadev.dictator.dao.item.Day;
import com.alphadev.dictator.dao.item.Session;
import com.alphadev.dictator.dao.item.Task;

public interface DataListener {

	public void onSetCurrentSession(Session session);

	public void onSetWorkingTasks(List<Task> tasks);

	public void onSetTodayCloseSession(List<Session> todayCloseSession);

	public void onSetWeekSummary(Map<Day, List<Session>> weekSummary);

	public void onChangeSession(Map<String, Session> sessions);

	public void onChangeTasks(Map<String, Task> tasks);

}
