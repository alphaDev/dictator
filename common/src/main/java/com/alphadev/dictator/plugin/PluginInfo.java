package com.alphadev.dictator.plugin;

import java.io.Serializable;

import org.apache.commons.lang3.StringUtils;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javafx.scene.Node;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

public class PluginInfo implements Serializable {
	private static final long	serialVersionUID	= -2060412409668602659L;
	private String				name;
	private String				logoPath;

	@JsonIgnore
	private Node				logo;

	public PluginInfo() {
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null || !(obj instanceof PluginInfo)) {
			return false;
		}
		PluginInfo pi = (PluginInfo) obj;
		return StringUtils.equals(name, pi.name) && StringUtils.equals(logoPath, pi.logoPath);
	}

	@Override
	public int hashCode() {
		return name.hashCode() + logoPath.hashCode();
	}

	public PluginInfo(String name, String logoPath) {
		super();
		this.name = name;
		this.logoPath = logoPath;
	}

	public String getName() {
		return name;
	}

	public String getLogoPath() {
		return logoPath;
	}

	public void setLogoPath(String logoPath) {
		this.logoPath = logoPath;
	}

	public Node getLogo() {
		if (logo == null) {
			Image image = new Image(PluginInfo.class.getResourceAsStream(logoPath));
			logo = new ImageView(image);
		}
		return logo;
	}

}
