package com.alphadev.dictator.plugin;

import java.util.Date;
import java.util.List;

import com.alphadev.dictator.dao.item.Session;
import com.alphadev.dictator.dao.item.Task;
import com.alphadev.dictator.dao.item.User;
import com.alphadev.dictator.dao.item.property.PropertyInfo;
import com.alphadev.dictator.properties.PropertyDefinition;

public interface Plugin {

	public PluginInfo getInfo();

	public void init();

	public List<PropertyDefinition> getPropertyDefinition();

	public List<PropertyInfo> askCustomProperties(Class<?> itemClass);

	public void setUser(User user);

	public void updateUser(User user);

	public void syncTasks(List<Task> existingTasks, boolean syncAll, Date from, Date to);

	public void syncSession(List<Task> tasks, List<Session> sessions, boolean syncAll, Date from, Date to);

	public Session createSession(Task task, Date start, Date end, String description);

	public Session closeSession(Session session);

	public Session updateSession(Session session);

	public void deleteSession(Session session);

}
