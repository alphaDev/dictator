package com.alphadev.dictator;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.alphadev.dictator.exception.DictatorRuntimeException;

/**
 * Format %[flag][width]format[delimiter]% flag : o : optional 0 : zero padded p : (part from upper unit) w : (part from working day unit) width : int : with format : W : working day d : day H : hour M : minute S : second delimiter : str : delimiter
 * 
 * @author LinkTheHero
 *
 */
public class DurationFormater {
	public static final Logger				LOG						= LogManager.getLogger(DurationFormater.class);

	private static final DurationFormater	instance				= new DurationFormater();

	public static Long						MILLIS_IN_WORKING_DAYS	= 1000l * 60l * 60l * 7l;
	public static final long				MILLIS_IN_DAY			= 1000l * 60l * 60l * 24l;
	public static final long				MILLIS_IN_HOURS			= 1000l * 60l * 60l;
	public static final long				MILLIS_IN_MINUTES		= 1000l * 60l;
	public static final long				MILLIS_IN_SECONDS		= 1000l;

	public static final String				DEFAULT_DURATION		= "%oHh%%poMm%%poSs%";

	// public static final String DURATION_FORMAT_WITH_WORKING_DAY = "%oWd%%woHh%%poMm%%poSs%";
	// public static final String DURATION_FORMAT_HOURS = "%oHh%%poMm%%poSs%";
	// public static final String DURATION_FORMAT_WITH_WORKING_DAY_NEON = "%oWd%w02H:%%p02M:%%p02S%";
	// public static final String DURATION_FORMAT_HOURS_NEON = "%02H:%%p02M:%%p02S%";

	private String format(String format, long duration) {
		if (duration <= 0) {
			return "-";
		}

		Token[] tokens = tokenize(format);
		ArrayUtils.reverse(tokens);
		StringBuffer buffer = new StringBuffer();

		for (Token token : tokens) {
			if (!token.flagOptional || (token.flagOptional && token.hasValue(duration))) {
				final String format2 = token.format(duration);
				buffer.insert(0, format2);
			}
		}

		return buffer.toString();
	}

	private Token[] tokenize(String format) {
		char oldchar = '-';
		boolean inToken = false;
		StringBuffer buffer = new StringBuffer();
		List<Token> tokens = new ArrayList<>();
		for (char c : format.toCharArray()) {
			switch (c) {
				case '%':
					if (oldchar != '%') {
						if (inToken) {
							tokens.add(new Token(buffer.toString()));
							inToken = false;
						}
						else {
							buffer = new StringBuffer();
							inToken = true;
						}
					}

				break;

				default:
					if (inToken) {
						buffer.append(c);
					}
					oldchar = c;
				break;
			}
		}
		return tokens.toArray(new Token[0]);
	}

	private class Token {
		boolean	flagZeroPadded	= false;
		boolean	flagOptional	= false;
		boolean	flagPart		= false;
		boolean	flagWorkingDay	= false;
		int		width			= 0;
		char	format;
		String	delimiter		= "";
		String	internalFormat	= "";

		public Token(String content) {
			width = 0;
			format = '-';
			delimiter = "";

			char[] contents = content.toCharArray();
			char currentChar = '-';

			boolean inFlag = true;
			boolean inWidth = false;
			boolean inFormat = false;
			boolean inDelimiter = false;
			StringBuffer buffer = new StringBuffer();
			for (int i = 0; i < contents.length;) {
				currentChar = contents[i];
				if (inFlag) {
					switch (currentChar) {
						case '0':
							flagZeroPadded = true;
							i++;
							continue;
						case 'o':
							flagOptional = true;
							i++;
							continue;
						case 'p':
							flagPart = true;
							i++;
							continue;
						case 'w':
							flagPart = true;
							flagWorkingDay = true;
							i++;
							continue;

						default:
							inFlag = false;
							inWidth = true;
						break;
					}
				}
				if (inWidth) {
					switch (currentChar) {
						case '0':
						case '1':
						case '2':
						case '3':
						case '4':
						case '5':
						case '6':
						case '7':
						case '8':
						case '9':
							buffer.append(currentChar);
							i++;
							continue;

						default:
							if (buffer.length() > 0) {
								width = Integer.parseInt(buffer.toString());
								buffer = new StringBuffer();
							}
							inWidth = false;
							inFormat = true;
						break;
					}
				}

				if (inFormat) {
					switch (currentChar) {
						case 'W':
						case 'd':
						case 'H':
						case 'h':
						case 'M':
						case 'S':
							format = currentChar;
							i++;
							inFormat = false;
							inDelimiter = true;
							continue;

						default:
							throw new DictatorRuntimeException("Missing format");
					}
				}

				if (inDelimiter) {
					buffer.append(currentChar);
					i++;
				}

			}

			delimiter = buffer.toString();

			internalFormat = "%" + (flagZeroPadded ? "0" : "") + (width == 0 ? "" : width) + "d" + "%s";
		}

		public boolean hasValue(long duration) {
			switch (format) {
				case 'W':
					return (duration > MILLIS_IN_WORKING_DAYS);
				case 'd':
					return (duration > MILLIS_IN_DAY);
				case 'H':
					return (duration > MILLIS_IN_HOURS);
				case 'M':
					return (duration > MILLIS_IN_MINUTES);
				case 'S':
					return (duration > MILLIS_IN_SECONDS);
			}
			return false;
		}

		public String format(long duration) {
			long value = 0;

			switch (format) {
				case 'W':
					value = duration / MILLIS_IN_WORKING_DAYS;
				break;
				case 'd':
					value = duration / MILLIS_IN_DAY;
				break;
				case 'H':
					if (flagPart) {
						if (flagWorkingDay) {
							value = (duration % MILLIS_IN_WORKING_DAYS);
						}
						else {
							value = (duration % MILLIS_IN_DAY);
						}
					}
					else {
						value = duration;
					}
					value = value / (MILLIS_IN_HOURS);
				break;
				case 'M':
					if (flagPart) {
						value = (duration % MILLIS_IN_HOURS);
					}
					else {
						value = duration;
					}
					value = value / MILLIS_IN_MINUTES;
				break;
				case 'S':
					if (flagPart) {
						value = (duration % MILLIS_IN_MINUTES);
					}
					else {
						value = duration;
					}
					value = value / MILLIS_IN_SECONDS;
				break;
			}

			return String.format(internalFormat, value, delimiter);

		}

		@Override
		public String toString() {
			return String.format("format:[%s] delimiter:[%s] internalFormat: [%s] width:[%d] flagZeroPadded:%b,flagOptional:%b,flagPart:%b,flagWorkingDay:%b,", format, delimiter, internalFormat, width, flagZeroPadded, flagOptional, flagPart, flagWorkingDay);
		}
	}

	public static void setWorkingDayDuration(Long duration) {
		MILLIS_IN_WORKING_DAYS = duration;
	}

	public static String formatDuration(String format, Long duration) {
		return instance.format(format, duration);
	}

	public static String format(Long duration) {
		return instance.format(DEFAULT_DURATION, duration);
	}
}
